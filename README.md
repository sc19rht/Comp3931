<!-- TITLE -->
<div align="center">
    <h1 align="center">COMP 3931 Visualizing Data Quality</h1>
        <p align="center">
    Author : Ren Hao Tan
    201343961
        <br />
    2021/2022
  </p>
</div>

<!-- ABOUT THE PROJECT -->
## About The Project

This project provides visualization on certain data qualities which can be scaled up to 9 different csv files.

### Built With

* [Python](https://www.python.org/downloads/)
* [Tkinter](https://docs.python.org/3/library/tkinter.html)
* [Matplotlib](https://matplotlib.org/)
* [Pandas](https://pandas.pydata.org/)
* [Ubuntu](https://ubuntu.com/)

<!-- GETTING STARTED -->
## Getting Started

1. Clone repository

    ```sh
    git clone https://gitlab.com/sc19rht/Comp3931.git
    ```

2. Change directory into repository from desktop

    ```sh
    cd Comp3931
    ```

3. Install virtualenv

    ```sh
    pip install virtualenv
    ```

4. Install setup tools

    ```sh
    virtualenv venv
    ```

5. Activate virtual environment

    ```sh
    source venv/bin/activate
    ```

6. Install dependencies

    ```sh
    pip install -r requirements.txt
    ```

7. Run program

    ```sh
    python app.py
    ```
