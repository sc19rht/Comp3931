import tkinter as tk
import pandas as pd
import matplotlib
import matplotlib.pyplot as plt
import os
from tkinter import *
from tkinter import CENTER, filedialog, messagebox
from matplotlib.backends.backend_tkagg import FigureCanvasTkAgg

matplotlib.use("TkAgg")

dictOfFiles = []
final_filenames = []

class App(tk.Frame):
    
    def __init__(self,parent):
        tk.Frame.__init__(self,parent)
        self.parent = parent
        self.initialize_user_interface()

    def initialize_user_interface(self):
        """Includes naming and respective widget creation in the initial window"""

        # Set Sizing Of Window
        self.parent.geometry("600x400") 
        self.parent.resizable(0, 0)

        # Window Title
        self.parent.title("Visualizing Data Quality") 

        # Headings
        self.parent.mainTitle = Label(self.parent, text = "Visualization of Data Quality", font = ('Helvetica 25 bold'))
        self.parent.mainTitle.pack(pady = 50)

        # Frame For Open File Dialog
        self.parent.file_frame = tk.LabelFrame(self.parent, text="File Selected")
        self.parent.file_frame.place(height = 250, width = 450, relx = 0.5, rely = 0.68, anchor=CENTER)

        # Radio Buttons
        self.r = IntVar()
        self.r.set("1") # Set Default Selected Radio Button to Completeness
        self.flag = 1

        self.parent.radiobutton1 = tk.Radiobutton(self.parent, text = "Completeness", variable = self.r, value = 1, command =lambda: self.rb1selected())
        self.parent.radiobutton1.place(height= 20, width = 120, relx = 0.140, rely = 0.27)

        self.parent.radiobutton2 = tk.Radiobutton(self.parent, text = "Consistency", variable = self.r, value = 2, command=lambda: self.rb2selected())
        self.parent.radiobutton2.place(height= 20, width = 110, relx = 0.395, rely = 0.27) 

        self.parent.radiobutton3 = tk.Radiobutton(self.parent, text = "Uniqueness", variable = self.r, value = 3, command=lambda: self.rb3selected())
        self.parent.radiobutton3.place(height= 20, width = 110, relx = 0.640, rely = 0.27)  

        # Browse A File Button
        self.button1 = tk.Button(self.parent.file_frame, text="Browse A File", command=lambda: self.File_dialog())
        self.button1.place(rely=0.85, relx=0.55)
        
        # Load File Button
        self.button2 = tk.Button(self.parent.file_frame, text="Load File", command=lambda: self.create_window())
        self.button2.place(rely=0.85, relx=0.35)

        # Clear Button
        self.button3 = tk.Button(self.parent.file_frame, text="Clear", command=lambda: self.clearFiles())
        self.button3.place(rely=0.85, relx=0.205)

    def rb1selected(self):
        """Flag sets to 1 when completeness radio button selected"""
        self.flag = 1
    
    def rb2selected(self):
        """Flag sets to 2 when completeness radio button selected"""
        self.flag = 2
    
    def rb3selected(self):
        """Flag sets to 3 when completeness radio button selected"""
        self.flag = 3

    # Function To Open New Window
    def create_window(self):
        """Creates a new window after clicking load button"""
        if len(dictOfFiles) != 0:
            # Initialize New Window
            self.window = tk.Toplevel()
            # Get User Screen Resolution
            self.screen_width = tk.Frame.winfo_screenwidth(self)
            self.screen_height = tk.Frame.winfo_screenheight(self)
            # Set New Window Size
            self.window.geometry("{}x{}".format(self.screen_width, self.screen_height))
            self.frame = tk.Frame(self.window)
            self.frame.pack(fill="both", expand=True)

            if self.flag == 1: # When Completeness Radio Button Is Selected
                self.label = tk.Label(self.frame, text = "Completeness", font = ("", 20))
                self.label.pack(padx = 20, pady = 5)
                self.run = self.Load_excel_data()
                Completeness().plotBarh(self.window)
                Completeness.completeDF.clear()
            
            elif self.flag == 2: # When Consistency Radio Button Is Selected
                self.label = tk.Label(self.frame, text = "Consistency ", font = ("", 20))
                self.label.pack(padx = 20, pady = 5)
                self.run = self.Load_excel_data()
                Consistency().plotWhiskanddots(self.window)
                Consistency.totalValueLength.clear()
                Consistency.consistDF.clear()

            elif self.flag == 3: # When Uniqueness Radio Button Is Selected
                self.label = tk.Label(self.frame, text = "Uniqueness ", font = ("", 20))
                self.label.pack(padx = 20, pady = 5)
                self.run = self.Load_excel_data()
                Uniqueness().plotuniquebarh(self.window)
                Uniqueness.uniqueDF.clear()
        else:
            messagebox.showerror("Error", "Please select a file")

    def clearFiles(self):
        """Clears list elements"""
        j = 0 
        for i in range(len(final_filenames)): # Clear Selected File Display
            lengthName = len(final_filenames[i])
            self.label_file = tk.Label(self.parent.file_frame, text = "   " * lengthName)
            self.label_file.place(rely= 0 + j , relx=0)
            j += 0.08
        dictOfFiles.clear() # Clear File paths list
        Completeness.completeDF.clear() # Clear Respective Dataframe List
        Consistency.consistDF.clear()
        Uniqueness.uniqueDF.clear()
        final_filenames.clear() # Clear Filenames List
        
    def File_dialog(self):
        """Allows user to select only CSV type files and store selected file in a list"""
        if len(dictOfFiles) < 9: # Allow User To Select A Maximum Of 9 Files
            self.filename = list(filedialog.askopenfilenames(title = "Select A File", filetypes = (("CSV Files","*.csv"),))) 
            for name in self.filename:
                if not name in dictOfFiles: # Add File Path Into List If Not Added Before
                    dictOfFiles.append(name)
            if len(dictOfFiles) >= 10:
                del dictOfFiles[9:]
                messagebox.showerror("Error", "Too many files selected. Maximum of 9 files only.")
        else:
            messagebox.showerror("Error", "Maximum of 9 files selected")
        j = 0
        x = '1'
        for i in range(len(dictOfFiles)): # Create Labels For Selected Files in File Dialog 
            self.exact_filename = os.path.basename(dictOfFiles[i]) # Get File Name
            if not self.exact_filename in final_filenames: # Store File Names In A List
                    final_filenames.append(self.exact_filename)
                    self.label_file = tk.Label(self.parent.file_frame, text = chr(ord(x)) + ") " + final_filenames[i])
                    self.label_file.place(rely= 0 + j , relx = 0)
            j += 0.08
            x = chr(ord(x)+ 1)        

    def Load_excel_data(self):
        """Read and store selected CSV files as dataframe"""
        for i in range(len(dictOfFiles)):
            try:
                excel_filename = r"{}".format(dictOfFiles[i]) # Read File   
                df = pd.read_csv(excel_filename, dtype='unicode') # Create Dataframe
                df = df.loc[:, ~df.columns.str.contains('^Unnamed')] # Remove 'Unnamed' Columns 

                if self.flag == 1:
                    Completeness().calMissingvalues(df)
                if self.flag == 2:
                    Consistency().calvalueslength(df)
                if self.flag == 3:
                    Uniqueness().caluniqueness(df)

            except ValueError:
                tk.messagebox.showerror("Information", "The file you have chosen is invalid")
                return None
            except FileNotFoundError:
                tk.messagebox.showerror("Information", f"No such file as {dictOfFiles[i]}")
                return None
######################################################## UNIQUENESS #######################################################################
class Uniqueness():
    
    uniqueDF = []

    def caluniqueness(self, df):
        """Find number of distinct values and non missing values"""
        self.unique_Values = []
        self.uniqueVariable = list(df.columns.values.tolist()) # Create A List Of All Column Names
        self.numofValues = list(df.notnull().sum()) # Find And List Number Of Non Empty Values
        self.distinctValues = list(df[self.uniqueVariable].nunique(dropna = True)) # Find And List Number Of Distinct Values
        for i, j in zip(self.distinctValues, self.numofValues):
            self.unique = round(((i - 1) / (j - 1)),2)
            self.unique_Values.append(self.unique)
        self.uniqueDict = {'Variables':self.uniqueVariable,'Uniqueness':self.unique_Values}
        self.df = pd.DataFrame(data = self.uniqueDict) # Create Dataframe with Data 'UniqueDict' Dictionary
        self.uniqueDF.append(self.df) # Add Dataframe Into A List

############################################## UNIQUENESS HORIZONTAL BAR CHART PLOTS #####################################################
    def plotuniquebarh(self,window):
        """Plot horizontal bar and dot charts onto the new window"""
        counter = len(self.uniqueDF)
        f = plt.Figure(figsize = (10,20))
        canvas = FigureCanvasTkAgg(f, window)
        canvas.get_tk_widget().pack(side = tk.TOP, fill = tk.BOTH, expand = True)
        if counter == 1:
            axes = f.add_subplot(1,1,1)
            axes.set_title(final_filenames[0], fontsize = 15, weight = 'bold')
            axes.set_xlabel('Uniqueness', fontsize = 15, weight = 'bold')
            axes.set_ylabel('Variables', fontsize = 15, weight = 'bold')

            self.uniqueDF[0].plot(kind = 'barh',
                        legend = False,
                        ax = axes,
                        x = 'Variables',
                        y = 'Uniqueness',
                        xlim = (0.0,1.0))

        if counter == 2:
            axes1 = f.add_subplot(1,2,1)
            axes1.set_title(final_filenames[0], fontsize = 15, weight = 'bold')
            axes1.set_xlabel('Uniqueness', fontsize = 15, weight = 'bold')
            axes1.set_ylabel('Variables', fontsize = 15, weight = 'bold')

            self.uniqueDF[0].plot(kind = 'barh',
                        legend = False,
                        ax = axes1,
                        x = 'Variables',
                        y = 'Uniqueness',
                        xlim = (0.0,1.0))

            axes2 = f.add_subplot(1,2,2, sharey = axes1)
            axes2.set_title(final_filenames[1], fontsize = 15, weight = 'bold')
            axes2.set_xlabel('Uniqueness', fontsize = 15, weight = 'bold')
            axes2.set_ylabel('Variables', fontsize = 15, weight = 'bold')

            self.uniqueDF[1].plot(kind = 'barh',
                        legend = False,
                        ax = axes2,
                        x = 'Variables',
                        y = 'Uniqueness',
                        xlim = (0.0,1.0))

        if counter == 3:
            axes1 = f.add_subplot(1,3,1)
            axes1.set_title(final_filenames[0], fontsize = 15, weight = 'bold')
            axes1.set_xlabel('Uniqueness', fontsize = 15, weight = 'bold')
            axes1.set_ylabel('Variables', fontsize = 15, weight = 'bold')

            self.uniqueDF[0].plot(kind = 'barh',
                        legend = False,
                        ax = axes1,
                        x = 'Variables',
                        y = 'Uniqueness',
                        xlim = (0.0,1.0))

            axes2 = f.add_subplot(1,3,2, sharey = axes1)
            axes2.set_title(final_filenames[1], fontsize = 15, weight = 'bold')
            axes2.set_xlabel('Uniqueness', fontsize = 15, weight = 'bold')
            axes2.set_ylabel('Variables', fontsize = 15, weight = 'bold')

            self.uniqueDF[1].plot(kind = 'barh',
                        legend = False,
                        ax = axes2,
                        x = 'Variables',
                        y = 'Uniqueness',
                        xlim = (0.0,1.0))

            axes3 = f.add_subplot(1,3,3, sharey = axes1)
            axes3.set_title(final_filenames[2], fontsize = 15, weight = 'bold')
            axes3.set_xlabel('Uniqueness', fontsize = 15, weight = 'bold')
            axes3.set_ylabel('Variables', fontsize = 15, weight = 'bold')

            self.uniqueDF[2].plot(kind = 'barh',
                        legend = False,
                        ax = axes3,
                        x = 'Variables',
                        y = 'Uniqueness',
                        xlim = (0.0,1.0))

        if counter == 4:
            axes1 = f.add_subplot(2,2,1)
            axes1.set_title(final_filenames[0], fontsize = 15, weight = 'bold')
            axes1.set_ylabel('Variables', fontsize = 15, weight = 'bold')

            self.uniqueDF[0].plot(kind = 'barh',
                        legend = False,
                        ax = axes1,
                        x = 'Variables',
                        y = 'Uniqueness',
                        xlim = (0.0,1.0))

            axes2 = f.add_subplot(2,2,2, sharey = axes1)
            axes2.set_title(final_filenames[1], fontsize = 15, weight = 'bold')

            self.uniqueDF[1].plot(kind = 'barh',
                        legend = False,
                        ax = axes2,
                        x = 'Variables',
                        y = 'Uniqueness',
                        xlim = (0.0,1.0))

            axes3 = f.add_subplot(2,2,3, sharey = axes1)
            axes3.set_title(final_filenames[2], fontsize = 15, weight = 'bold')
            axes3.set_xlabel('Uniqueness', fontsize = 15, weight = 'bold')
            axes3.set_ylabel('Variables', fontsize = 15, weight = 'bold')

            self.uniqueDF[2].plot(kind = 'barh',
                        legend = False,
                        ax = axes3,
                        x = 'Variables',
                        y = 'Uniqueness',
                        xlim = (0.0,1.0))
            
            axes4 = f.add_subplot(2,2,4, sharey = axes1)
            axes4.set_title(final_filenames[3], fontsize = 15, weight = 'bold')
            axes4.set_xlabel('Uniqueness', fontsize = 15, weight = 'bold')
            axes4.set_ylabel('Variables', fontsize = 15, weight = 'bold')

            self.uniqueDF[3].plot(kind = 'barh',
                        legend = False,
                        ax = axes4,
                        x = 'Variables',
                        y = 'Uniqueness',
                        xlim = (0.0,1.0))
        
        if counter == 5:
            axes1 = f.add_subplot(2,3,1)
            axes1.set_title(final_filenames[0], fontsize = 15, weight = 'bold')
            axes1.set_xlabel('Uniqueness', fontsize = 15, weight = 'bold')
            axes1.set_ylabel('Variables', fontsize = 15, weight = 'bold')
            f.subplots_adjust(hspace = 0.3)

            self.uniqueDF[0].plot(kind = 'barh',
                        legend = False,
                        ax = axes1,
                        x = 'Variables',
                        y = 'Uniqueness',
                        xlim = (0.0,1.0))

            axes2 = f.add_subplot(2,3,2, sharey = axes1)
            axes2.set_title(final_filenames[1], fontsize = 15, weight = 'bold')
            axes2.set_xlabel('Uniqueness', fontsize = 15, weight = 'bold')

            self.uniqueDF[1].plot(kind = 'barh',
                        legend = False,
                        ax = axes2,
                        x = 'Variables',
                        y = 'Uniqueness',
                        xlim = (0.0,1.0))

            axes3 = f.add_subplot(2,3,3, sharey = axes1)
            axes3.set_title(final_filenames[2], fontsize = 15, weight = 'bold')
            axes3.set_xlabel('Uniqueness', fontsize = 15, weight = 'bold')
            axes3.set_ylabel('Variables', fontsize = 15, weight = 'bold')

            self.uniqueDF[2].plot(kind = 'barh',
                        legend = False,
                        ax = axes3,
                        x = 'Variables',
                        y = 'Uniqueness',
                        xlim = (0.0,1.0))
            
            axes4 = f.add_subplot(2,2,3)
            axes4.set_title(final_filenames[3], fontsize = 15, weight = 'bold')
            axes4.set_xlabel('Uniqueness', fontsize = 15, weight = 'bold')
            axes4.set_ylabel('Variables', fontsize = 15, weight = 'bold')

            self.uniqueDF[3].plot(kind = 'barh',
                        legend = False,
                        ax = axes4,
                        x = 'Variables',
                        y = 'Uniqueness',
                        xlim = (0.0,1.0))
            
            axes5 = f.add_subplot(2,2,4, sharey = axes4)
            axes5.set_title(final_filenames[4], fontsize = 15, weight = 'bold')
            axes5.set_xlabel('Uniqueness', fontsize = 15, weight = 'bold')

            self.uniqueDF[4].plot(kind = 'barh',
                        legend = False,
                        ax = axes5,
                        x = 'Variables',
                        y = 'Uniqueness',
                        xlim = (0.0,1.0))

        if counter == 6:
            axes1 = f.add_subplot(2,3,1)
            axes1.set_title(final_filenames[0], fontsize = 15, weight = 'bold')
            axes1.set_ylabel('Variables', fontsize = 15, weight = 'bold')

            self.uniqueDF[0].plot(kind = 'barh',
                        legend = False,
                        ax = axes1,
                        x = 'Variables',
                        y = 'Uniqueness',
                        xlim = (0.0,1.0))

            axes2 = f.add_subplot(2,3,2, sharey = axes1)
            axes2.set_title(final_filenames[1], fontsize = 15, weight = 'bold')

            self.uniqueDF[1].plot(kind = 'barh',
                        legend = False,
                        ax = axes2,
                        x = 'Variables',
                        y = 'Uniqueness',
                        xlim = (0.0,1.0))

            axes3 = f.add_subplot(2,3,3, sharey = axes1)
            axes3.set_title(final_filenames[2], fontsize = 15, weight = 'bold')
            axes3.set_ylabel('Variables', fontsize = 15, weight = 'bold')

            self.uniqueDF[2].plot(kind = 'barh',
                        legend = False,
                        ax = axes3,
                        x = 'Variables',
                        y = 'Uniqueness',
                        xlim = (0.0,1.0))
            
            axes4 = f.add_subplot(2,3,4)
            axes4.set_title(final_filenames[3], fontsize = 15, weight = 'bold')
            axes4.set_xlabel('Uniqueness', fontsize = 15, weight = 'bold')
            axes4.set_ylabel('Variables', fontsize = 15, weight = 'bold')

            self.uniqueDF[3].plot(kind = 'barh',
                        legend = False,
                        ax = axes4,
                        x = 'Variables',
                        y = 'Uniqueness',
                        xlim = (0.0,1.0))
            
            axes5 = f.add_subplot(2,3,5, sharey = axes4)
            axes5.set_title(final_filenames[4], fontsize = 15, weight = 'bold')
            axes5.set_xlabel('Uniqueness', fontsize = 15, weight = 'bold')

            self.uniqueDF[4].plot(kind = 'barh',
                        legend = False,
                        ax = axes5,
                        x = 'Variables',
                        y = 'Uniqueness',
                        xlim = (0.0,1.0))
            
            axes6 = f.add_subplot(2,3,6, sharey = axes4)
            axes6.set_title(final_filenames[5], fontsize = 15, weight = 'bold')
            axes6.set_xlabel('Uniqueness', fontsize = 15, weight = 'bold')

            self.uniqueDF[5].plot(kind = 'barh',
                        legend = False,
                        ax = axes6,
                        x = 'Variables',
                        y = 'Uniqueness',
                        xlim = (0.0,1.0))
        
        if counter == 7:
            axes1 = f.add_subplot(3,3,1)
            axes1.set_title(final_filenames[0], fontsize = 15, weight = 'bold')
            axes1.set_ylabel('Variables', fontsize = 15, weight = 'bold')
            axes1.set_xlabel('Uniqueness', fontsize = 15, weight = 'bold')
            f.subplots_adjust(hspace = 0.5)

            self.uniqueDF[0].plot(kind = 'barh',
                        legend = False,
                        ax = axes1,
                        x = 'Variables',
                        y = 'Uniqueness',
                        xlim = (0.0,1.0))

            axes2 = f.add_subplot(3,3,2, sharey = axes1)
            axes2.set_title(final_filenames[1], fontsize = 15, weight = 'bold')
            axes2.set_xlabel('Uniqueness', fontsize = 15, weight = 'bold')

            self.uniqueDF[1].plot(kind = 'barh',
                        legend = False,
                        ax = axes2,
                        x = 'Variables',
                        y = 'Uniqueness',
                        xlim = (0.0,1.0))

            axes3 = f.add_subplot(3,3,3, sharey = axes1)
            axes3.set_title(final_filenames[2], fontsize = 15, weight = 'bold')
            axes3.set_xlabel('Uniqueness', fontsize = 15, weight = 'bold')

            self.uniqueDF[2].plot(kind = 'barh',
                        legend = False,
                        ax = axes3,
                        x = 'Variables',
                        y = 'Uniqueness',
                        xlim = (0.0,1.0))
            
            axes4 = f.add_subplot(3,2,3)
            axes4.set_title(final_filenames[3], fontsize = 15, weight = 'bold')
            axes4.set_xlabel('Uniqueness', fontsize = 15, weight = 'bold')
            axes4.set_ylabel('Variables', fontsize = 15, weight = 'bold')

            self.uniqueDF[3].plot(kind = 'barh',
                        legend = False,
                        ax = axes4,
                        x = 'Variables',
                        y = 'Uniqueness',
                        xlim = (0.0,1.0))
            
            axes5 = f.add_subplot(3,2,4, sharey = axes4)
            axes5.set_title(final_filenames[4], fontsize = 15, weight = 'bold')
            axes5.set_xlabel('Uniqueness', fontsize = 15, weight = 'bold')

            self.uniqueDF[4].plot(kind = 'barh',
                        legend = False,
                        ax = axes5,
                        x = 'Variables',
                        y = 'Uniqueness',
                        xlim = (0.0,1.0))
            
            axes6 = f.add_subplot(3,2,5)
            axes6.set_title(final_filenames[5], fontsize = 15, weight = 'bold')
            axes6.set_xlabel('Uniqueness', fontsize = 15, weight = 'bold')
            axes6.set_ylabel('Variables', fontsize = 15, weight = 'bold')

            self.uniqueDF[5].plot(kind = 'barh',
                        legend = False,
                        ax = axes6,
                        x = 'Variables',
                        y = 'Uniqueness',
                        xlim = (0.0,1.0))

            axes7 = f.add_subplot(3,2,6, sharey = axes6)
            axes7.set_title(final_filenames[6], fontsize = 15, weight = 'bold')
            axes7.set_xlabel('Uniqueness', fontsize = 15, weight = 'bold')

            self.uniqueDF[6].plot(kind = 'barh',
                        legend = False,
                        ax = axes7,
                        x = 'Variables',
                        y = 'Uniqueness',
                        xlim = (0.0,1.0))    

        if counter == 8:
            axes1 = f.add_subplot(3,3,1)
            axes1.set_title(final_filenames[0], fontsize = 15, weight = 'bold')
            axes1.set_ylabel('Variables', fontsize = 15, weight = 'bold')
            axes1.set_xlabel('Uniqueness', fontsize = 15, weight = 'bold')
            f.subplots_adjust(hspace = 0.5)

            self.uniqueDF[0].plot(kind = 'barh',
                        legend = False,
                        ax = axes1,
                        x = 'Variables',
                        y = 'Uniqueness',
                        xlim = (0.0,1.0))

            axes2 = f.add_subplot(3,3,2, sharey = axes1)
            axes2.set_title(final_filenames[1], fontsize = 15, weight = 'bold')
            axes2.set_xlabel('Uniqueness', fontsize = 15, weight = 'bold')

            self.uniqueDF[1].plot(kind = 'barh',
                        legend = False,
                        ax = axes2,
                        x = 'Variables',
                        y = 'Uniqueness',
                        xlim = (0.0,1.0))

            axes3 = f.add_subplot(3,3,3, sharey = axes1)
            axes3.set_title(final_filenames[2], fontsize = 15, weight = 'bold')
            axes3.set_xlabel('Uniqueness', fontsize = 15, weight = 'bold')

            self.uniqueDF[2].plot(kind = 'barh',
                        legend = False,
                        ax = axes3,
                        x = 'Variables',
                        y = 'Uniqueness',
                        xlim = (0.0,1.0))
            
            axes4 = f.add_subplot(3,3,4)
            axes4.set_title(final_filenames[3], fontsize = 15, weight = 'bold')
            axes4.set_xlabel('Uniqueness', fontsize = 15, weight = 'bold')
            axes4.set_ylabel('Variables', fontsize = 15, weight = 'bold')

            self.uniqueDF[3].plot(kind = 'barh',
                        legend = False,
                        ax = axes4,
                        x = 'Variables',
                        y = 'Uniqueness',
                        xlim = (0.0,1.0))
            
            axes5 = f.add_subplot(3,3,5, sharey = axes4)
            axes5.set_title(final_filenames[4], fontsize = 15, weight = 'bold')
            axes5.set_xlabel('Uniqueness', fontsize = 15, weight = 'bold')

            self.uniqueDF[4].plot(kind = 'barh',
                        legend = False,
                        ax = axes5,
                        x = 'Variables',
                        y = 'Uniqueness',
                        xlim = (0.0,1.0))
            
            axes6 = f.add_subplot(3,3,6, sharey = axes4)
            axes6.set_title(final_filenames[5], fontsize = 15, weight = 'bold')
            axes6.set_xlabel('Uniqueness', fontsize = 15, weight = 'bold')

            self.uniqueDF[5].plot(kind = 'barh',
                        legend = False,
                        ax = axes6,
                        x = 'Variables',
                        y = 'Uniqueness',
                        xlim = (0.0,1.0))

            axes7 = f.add_subplot(3,2,5)
            axes7.set_title(final_filenames[6], fontsize = 15, weight = 'bold')
            axes7.set_xlabel('Uniqueness', fontsize = 15, weight = 'bold')
            axes7.set_ylabel('Variables', fontsize = 15, weight = 'bold')

            self.uniqueDF[6].plot(kind = 'barh',
                        legend = False,
                        ax = axes7,
                        x = 'Variables',
                        y = 'Uniqueness',
                        xlim = (0.0,1.0))

            axes8 = f.add_subplot(3,2,6, sharey = axes7)
            axes8.set_title(final_filenames[7], fontsize = 15, weight = 'bold')
            axes8.set_xlabel('Uniqueness', fontsize = 15, weight = 'bold')

            self.uniqueDF[7].plot(kind = 'barh',
                        legend = False,
                        ax = axes8,
                        x = 'Variables',
                        y = 'Uniqueness',
                        xlim = (0.0,1.0))    

        if counter == 9:
            axes1 = f.add_subplot(3,3,1)
            axes1.set_title(final_filenames[0], fontsize = 15, weight = 'bold')
            axes1.set_ylabel('Variables', fontsize = 15, weight = 'bold')
            f.subplots_adjust(hspace = 0.4)

            self.uniqueDF[0].plot(kind = 'barh',
                        legend = False,
                        ax = axes1,
                        x = 'Variables',
                        y = 'Uniqueness',
                        xlim = (0.0,1.0))

            axes2 = f.add_subplot(3,3,2, sharey = axes1)
            axes2.set_title(final_filenames[1], fontsize = 15, weight = 'bold')

            self.uniqueDF[1].plot(kind = 'barh',
                        legend = False,
                        ax = axes2,
                        x = 'Variables',
                        y = 'Uniqueness',
                        xlim = (0.0,1.0))

            axes3 = f.add_subplot(3,3,3, sharey = axes1)
            axes3.set_title(final_filenames[2], fontsize = 15, weight = 'bold')

            self.uniqueDF[2].plot(kind = 'barh',
                        legend = False,
                        ax = axes3,
                        x = 'Variables',
                        y = 'Uniqueness',
                        xlim = (0.0,1.0))
            
            axes4 = f.add_subplot(3,3,4)
            axes4.set_title(final_filenames[3], fontsize = 15, weight = 'bold')
            axes4.set_ylabel('Variables', fontsize = 15, weight = 'bold')

            self.uniqueDF[3].plot(kind = 'barh',
                        legend = False,
                        ax = axes4,
                        x = 'Variables',
                        y = 'Uniqueness',
                        xlim = (0.0,1.0))
            
            axes5 = f.add_subplot(3,3,5, sharey = axes4)
            axes5.set_title(final_filenames[4], fontsize = 15, weight = 'bold')

            self.uniqueDF[4].plot(kind = 'barh',
                        legend = False,
                        ax = axes5,
                        x = 'Variables',
                        y = 'Uniqueness',
                        xlim = (0.0,1.0))
            
            axes6 = f.add_subplot(3,3,6, sharey = axes4)
            axes6.set_title(final_filenames[5], fontsize = 15, weight = 'bold')

            self.uniqueDF[5].plot(kind = 'barh',
                        legend = False,
                        ax = axes6,
                        x = 'Variables',
                        y = 'Uniqueness',
                        xlim = (0.0,1.0))

            axes7 = f.add_subplot(3,3,7)
            axes7.set_title(final_filenames[6], fontsize = 15, weight = 'bold')
            axes7.set_xlabel('Uniqueness', fontsize = 15, weight = 'bold')
            axes7.set_ylabel('Variables', fontsize = 15, weight = 'bold')

            self.uniqueDF[6].plot(kind = 'barh',
                        legend = False,
                        ax = axes7,
                        x = 'Variables',
                        y = 'Uniqueness',
                        xlim = (0.0,1.0))

            axes8 = f.add_subplot(3,3,8, sharey = axes7)
            axes8.set_title(final_filenames[7], fontsize = 15, weight = 'bold')
            axes8.set_xlabel('Uniqueness', fontsize = 15, weight = 'bold')

            self.uniqueDF[7].plot(kind = 'barh',
                        legend = False,
                        ax = axes8,
                        x = 'Variables',
                        y = 'Uniqueness',
                        xlim = (0.0,1.0))

            axes9 = f.add_subplot(3,3,9, sharey = axes7)
            axes9.set_title(final_filenames[8], fontsize = 15, weight = 'bold')
            axes9.set_xlabel('Uniqueness', fontsize = 15, weight = 'bold')

            self.uniqueDF[8].plot(kind = 'barh',
                        legend = False,
                        ax = axes9,
                        x = 'Variables',
                        y = 'Uniqueness',
                        xlim = (0.0,1.0))      
        
############################################################# CONSISTENCY ######################################################################
class Consistency(): 

    consistDF = []
    totalValueLength = []
    def calvalueslength(self, df):  
        """Find max and min length of the values in dataframe"""
        self.maxValueLength  = []
        self.minValueLength = []
        self.conVariable = list(df.columns.values.tolist()) # Create A List Of All Column Names
        df = df.fillna('') # Replace NaN Values With Empty String
        for col in range(len(df.columns)):
            self.maxValueLength.append(max(df.iloc[:,col].astype(str).apply(len)))
            self.minValueLength.append(min(df.iloc[:,col].astype(str).apply(len)))
        self.totalValueLength.extend(list(set(self.maxValueLength)) + list(set(self.minValueLength)))
        lengthDict = {'Variables':self.conVariable,'Min Length': self.minValueLength, 'Max Length':self.maxValueLength}
        self.df = pd.DataFrame(data = lengthDict) # Dataframe With Values From 'lengthDict' Dictionary 
        self.consistDF.append(self.df) # Add Dataframe Into A List
      
    def plotWhiskanddots(self,window):
        """Plot whiskers and dot charts on to new window"""
        counter = len(self.consistDF)
        f = plt.Figure(figsize = (10,20))
        canvas = FigureCanvasTkAgg(f, window)
        canvas.get_tk_widget().pack(side = tk.TOP, fill = tk.BOTH, expand = True)
        ticks = ([self.totalValueLength.pop(self.totalValueLength.index(max(self.totalValueLength)))] # Get Min and Max Tick for x-axis
                + [self.totalValueLength.pop(self.totalValueLength.index(min(self.totalValueLength)))])
####################################################### WHISKERS AND DOT PLOTS ###############################################################
        if counter == 1:
            axes = f.add_subplot(1,1,1)
            axes.set_title(final_filenames[0], fontsize = 15, weight = 'bold')
            axes.set_xlabel('Value Length', fontsize = 15, weight = 'bold')
            axes.set_ylabel('Variables', fontsize = 15, weight = 'bold')
            axes.set_xticks(ticks)
            
            for j in range(self.consistDF[0].shape[0]):
                axes.hlines(y = self.consistDF[0].iloc[j,0], xmin = self.consistDF[0].iloc[j,1] , xmax = self.consistDF[0].iloc[j,2], lw = 2, color = 'k')
                if self.consistDF[0].iloc[j,1] != self.consistDF[0].iloc[j,2]:
                    axes.plot(self.consistDF[0].iloc[j,1], self.consistDF[0].iloc[j,0], '|k', ms = 20, mew = 1.5)
                    axes.plot(self.consistDF[0].iloc[j,2], self.consistDF[0].iloc[j,0], '|k', ms = 20, mew = 1.5)
                else:
                    axes.plot(self.consistDF[0].iloc[j,2], self.consistDF[0].iloc[j,0], 'ok')
        
        if counter == 2:
            axes1 = f.add_subplot(1,2,1)
            axes1.set_title(final_filenames[0], fontsize = 15, weight = 'bold')
            axes1.set_xlabel('Value Length', fontsize = 15, weight = 'bold')
            axes1.set_ylabel('Variables', fontsize = 15, weight = 'bold')
            axes1.set_xticks(ticks)

            for j in range(self.consistDF[0].shape[0]):
                axes1.hlines(y = self.consistDF[0].iloc[j,0], xmin = self.consistDF[0].iloc[j,1] , xmax = self.consistDF[0].iloc[j,2], lw = 2, color = 'k')
                if self.consistDF[0].iloc[j,1] != self.consistDF[0].iloc[j,2]:
                    axes1.plot(self.consistDF[0].iloc[j,1], self.consistDF[0].iloc[j,0], '|k', ms = 20, mew = 1.5)
                    axes1.plot(self.consistDF[0].iloc[j,2], self.consistDF[0].iloc[j,0], '|k', ms = 20, mew = 1.5)
                else:
                    axes1.plot(self.consistDF[0].iloc[j,2], self.consistDF[0].iloc[j,0], 'ok')
                
            axes2 = f.add_subplot(1,2,2, sharex = axes1)
            axes2.set_title(final_filenames[1], fontsize = 15, weight = 'bold')
            axes2.set_xlabel('Value Length', fontsize = 15, weight = 'bold')
            axes2.yaxis.set_ticklabels([])
            
            for j in range(self.consistDF[1].shape[0]):
                axes2.hlines(y = self.consistDF[1].iloc[j,0], xmin = self.consistDF[1].iloc[j,1] , xmax = self.consistDF[1].iloc[j,2], lw = 2, color = 'k')
                if self.consistDF[1].iloc[j,1] != self.consistDF[1].iloc[j,2]:
                    axes2.plot(self.consistDF[1].iloc[j,1], self.consistDF[1].iloc[j,0], '|k', ms = 20, mew = 1.5)
                    axes2.plot(self.consistDF[1].iloc[j,2], self.consistDF[1].iloc[j,0], '|k', ms = 20, mew = 1.5)
                else:
                    axes2.plot(self.consistDF[1].iloc[j,2], self.consistDF[1].iloc[j,0], 'ok')
             
        if counter == 3:
            axes1 = f.add_subplot(1,3,1)
            axes1.set_title(final_filenames[0], fontsize = 15, weight = 'bold')
            axes1.set_xlabel('Value Length', fontsize = 15, weight = 'bold')
            axes1.set_ylabel('Variables', fontsize = 15, weight = 'bold')
            axes1.set_xticks(ticks)

            for j in range(self.consistDF[0].shape[0]):
                axes1.hlines(y = self.consistDF[0].iloc[j,0], xmin = self.consistDF[0].iloc[j,1] , xmax = self.consistDF[0].iloc[j,2], lw = 2, color = 'k')
                if self.consistDF[0].iloc[j,1] != self.consistDF[0].iloc[j,2]:
                    axes1.plot(self.consistDF[0].iloc[j,1], self.consistDF[0].iloc[j,0], '|k', ms = 20, mew = 1.5)
                    axes1.plot(self.consistDF[0].iloc[j,2], self.consistDF[0].iloc[j,0], '|k', ms = 20, mew = 1.5)
                else:
                    axes1.plot(self.consistDF[0].iloc[j,2], self.consistDF[0].iloc[j,0], 'ok')
                
            axes2 = f.add_subplot(1,3,2, sharex = axes1)
            axes2.set_title(final_filenames[1], fontsize = 15, weight = 'bold')
            axes2.set_xlabel('Value Length', fontsize = 15, weight = 'bold')
            axes2.yaxis.set_ticklabels([])

            for j in range(self.consistDF[1].shape[0]):
                axes2.hlines(y = self.consistDF[1].iloc[j,0], xmin = self.consistDF[1].iloc[j,1] , xmax = self.consistDF[1].iloc[j,2], lw = 2, color = 'k')
                if self.consistDF[1].iloc[j,1] != self.consistDF[1].iloc[j,2]:
                    axes2.plot(self.consistDF[1].iloc[j,1], self.consistDF[1].iloc[j,0], '|k', ms = 20, mew = 1.5)
                    axes2.plot(self.consistDF[1].iloc[j,2], self.consistDF[1].iloc[j,0], '|k', ms = 20, mew = 1.5)
                else:
                    axes2.plot(self.consistDF[1].iloc[j,2], self.consistDF[1].iloc[j,0], 'ok')

            axes3 = f.add_subplot(1,3,3, sharex = axes1)
            axes3.set_title(final_filenames[2], fontsize = 15, weight = 'bold')
            axes3.set_xlabel('Value Length', fontsize = 15, weight = 'bold')
            axes3.yaxis.set_ticklabels([])

            for j in range(self.consistDF[2].shape[0]):
                axes3.hlines(y = self.consistDF[2].iloc[j,0], xmin = self.consistDF[2].iloc[j,1] , xmax = self.consistDF[2].iloc[j,2], lw = 2, color = 'k')
                if self.consistDF[2].iloc[j,1] != self.consistDF[2].iloc[j,2]:
                    axes3.plot(self.consistDF[2].iloc[j,1], self.consistDF[2].iloc[j,0], '|k', ms = 20, mew = 1.5)
                    axes3.plot(self.consistDF[2].iloc[j,2], self.consistDF[2].iloc[j,0], '|k', ms = 20, mew = 1.5)
                else:
                    axes3.plot(self.consistDF[2].iloc[j,2], self.consistDF[2].iloc[j,0], 'ok')

        if counter == 4:

            axes1 = f.add_subplot(2,2,1)
            axes1.set_title(final_filenames[0], fontsize = 15, weight = 'bold')
            axes1.set_ylabel('Variables', fontsize = 15, weight = 'bold')
            axes1.set_xticks(ticks)

            for j in range(self.consistDF[0].shape[0]):
                axes1.hlines(y = self.consistDF[0].iloc[j,0], xmin = self.consistDF[0].iloc[j,1] , xmax = self.consistDF[0].iloc[j,2], lw = 2, color = 'k')
                if self.consistDF[0].iloc[j,1] != self.consistDF[0].iloc[j,2]:
                    axes1.plot(self.consistDF[0].iloc[j,1], self.consistDF[0].iloc[j,0], '|k', ms = 15, mew = 1.5)
                    axes1.plot(self.consistDF[0].iloc[j,2], self.consistDF[0].iloc[j,0], '|k', ms = 15, mew = 1.5)
                else:
                    axes1.plot(self.consistDF[0].iloc[j,2], self.consistDF[0].iloc[j,0], 'ok')
                
            axes2 = f.add_subplot(2,2,2, sharex = axes1)
            axes2.set_title(final_filenames[1], fontsize = 15, weight = 'bold')
            axes2.yaxis.set_ticklabels([])

            for j in range(self.consistDF[1].shape[0]):
                axes2.hlines(y = self.consistDF[1].iloc[j,0], xmin = self.consistDF[1].iloc[j,1] , xmax = self.consistDF[1].iloc[j,2], lw = 2, color = 'k')
                if self.consistDF[1].iloc[j,1] != self.consistDF[1].iloc[j,2]:
                    axes2.plot(self.consistDF[1].iloc[j,1], self.consistDF[1].iloc[j,0], '|k', ms = 15, mew = 1.5)
                    axes2.plot(self.consistDF[1].iloc[j,2], self.consistDF[1].iloc[j,0], '|k', ms = 15, mew = 1.5)
                else:
                    axes2.plot(self.consistDF[1].iloc[j,2], self.consistDF[1].iloc[j,0], 'ok')

            axes3 = f.add_subplot(2,2,3, sharex = axes1)
            axes3.set_title(final_filenames[2], fontsize = 15, weight = 'bold')
            axes3.set_xlabel('Value Length', fontsize = 15, weight = 'bold')
            axes3.set_ylabel('Variables', fontsize = 15, weight = 'bold')

            for j in range(self.consistDF[2].shape[0]):
                axes3.hlines(y = self.consistDF[2].iloc[j,0], xmin = self.consistDF[2].iloc[j,1] , xmax = self.consistDF[2].iloc[j,2], lw = 2, color = 'k')
                if self.consistDF[2].iloc[j,1] != self.consistDF[2].iloc[j,2]:
                    axes3.plot(self.consistDF[2].iloc[j,1], self.consistDF[2].iloc[j,0], '|k', ms = 15, mew = 1.5)
                    axes3.plot(self.consistDF[2].iloc[j,2], self.consistDF[2].iloc[j,0], '|k', ms = 15, mew = 1.5)
                else:
                    axes3.plot(self.consistDF[2].iloc[j,2], self.consistDF[2].iloc[j,0], 'ok')
            
            axes4 = f.add_subplot(2,2,4, sharex = axes1)
            axes4.set_title(final_filenames[3], fontsize = 15, weight = 'bold')
            axes4.set_xlabel('Value Length', fontsize = 15, weight = 'bold')
            axes4.yaxis.set_ticklabels([])

            for j in range(self.consistDF[3].shape[0]):
                axes4.hlines(y = self.consistDF[3].iloc[j,0], xmin = self.consistDF[3].iloc[j,1] , xmax = self.consistDF[3].iloc[j,2], lw = 2, color = 'k')
                if self.consistDF[3].iloc[j,1] != self.consistDF[3].iloc[j,2]:
                    axes4.plot(self.consistDF[3].iloc[j,1], self.consistDF[3].iloc[j,0], '|k', ms = 15, mew = 1.5)
                    axes4.plot(self.consistDF[3].iloc[j,2], self.consistDF[3].iloc[j,0], '|k', ms = 15, mew = 1.5)
                else:
                    axes4.plot(self.consistDF[3].iloc[j,2], self.consistDF[3].iloc[j,0], 'ok')
        
        if counter == 5:

            axes1 = f.add_subplot(2,3,1)
            axes1.set_title(final_filenames[0], fontsize = 15, weight = 'bold')
            axes1.set_xlabel('Value Length', fontsize = 15, weight = 'bold')
            axes1.set_ylabel('Variables', fontsize = 15, weight = 'bold')
            axes1.set_xticks(ticks)
            f.subplots_adjust(hspace = 0.3)

            for j in range(self.consistDF[0].shape[0]):
                axes1.hlines(y = self.consistDF[0].iloc[j,0], xmin = self.consistDF[0].iloc[j,1] , xmax = self.consistDF[0].iloc[j,2], lw = 2, color = 'k')
                if self.consistDF[0].iloc[j,1] != self.consistDF[0].iloc[j,2]:
                    axes1.plot(self.consistDF[0].iloc[j,1], self.consistDF[0].iloc[j,0], '|k', ms = 15, mew = 1.5)
                    axes1.plot(self.consistDF[0].iloc[j,2], self.consistDF[0].iloc[j,0], '|k', ms = 15, mew = 1.5)
                else:
                    axes1.plot(self.consistDF[0].iloc[j,2], self.consistDF[0].iloc[j,0], 'ok')
                
            axes2 = f.add_subplot(2,3,2, sharex = axes1)
            axes2.set_title(final_filenames[1], fontsize = 15, weight = 'bold')
            axes2.set_xlabel('Value Length', fontsize = 15, weight = 'bold')
            axes2.yaxis.set_ticklabels([])

            for j in range(self.consistDF[1].shape[0]):
                axes2.hlines(y = self.consistDF[1].iloc[j,0], xmin = self.consistDF[1].iloc[j,1] , xmax = self.consistDF[1].iloc[j,2], lw = 2, color = 'k')
                if self.consistDF[1].iloc[j,1] != self.consistDF[1].iloc[j,2]:
                    axes2.plot(self.consistDF[1].iloc[j,1], self.consistDF[1].iloc[j,0], '|k', ms = 15, mew = 1.5)
                    axes2.plot(self.consistDF[1].iloc[j,2], self.consistDF[1].iloc[j,0], '|k', ms = 15, mew = 1.5)
                else:
                    axes2.plot(self.consistDF[1].iloc[j,2], self.consistDF[1].iloc[j,0], 'ok')

            axes3 = f.add_subplot(2,3,3, sharex = axes1)
            axes3.set_title(final_filenames[2], fontsize = 15, weight = 'bold')
            axes3.set_xlabel('Value Length', fontsize = 15, weight = 'bold')
            axes3.yaxis.set_ticklabels([])
            
            for j in range(self.consistDF[2].shape[0]):
                axes3.hlines(y = self.consistDF[2].iloc[j,0], xmin = self.consistDF[2].iloc[j,1] , xmax = self.consistDF[2].iloc[j,2], lw = 2, color = 'k')
                if self.consistDF[2].iloc[j,1] != self.consistDF[2].iloc[j,2]:
                    axes3.plot(self.consistDF[2].iloc[j,1], self.consistDF[2].iloc[j,0], '|k', ms = 15, mew = 1.5)
                    axes3.plot(self.consistDF[2].iloc[j,2], self.consistDF[2].iloc[j,0], '|k', ms = 15, mew = 1.5)
                else:
                    axes3.plot(self.consistDF[2].iloc[j,2], self.consistDF[2].iloc[j,0], 'ok')
            
            axes4 = f.add_subplot(2,2,3, sharex = axes1)
            axes4.set_title(final_filenames[3], fontsize = 15, weight = 'bold')
            axes4.set_xlabel('Value Length', fontsize = 15, weight = 'bold')
            axes4.set_ylabel('Variables', fontsize = 15, weight = 'bold')

            for j in range(self.consistDF[3].shape[0]):
                axes4.hlines(y = self.consistDF[3].iloc[j,0], xmin = self.consistDF[3].iloc[j,1] , xmax = self.consistDF[3].iloc[j,2], lw = 2, color = 'k')
                if self.consistDF[3].iloc[j,1] != self.consistDF[3].iloc[j,2]:
                    axes4.plot(self.consistDF[3].iloc[j,1], self.consistDF[3].iloc[j,0], '|k', ms = 15, mew = 1.5)
                    axes4.plot(self.consistDF[3].iloc[j,2], self.consistDF[3].iloc[j,0], '|k', ms = 15, mew = 1.5)
                else:
                    axes4.plot(self.consistDF[3].iloc[j,2], self.consistDF[3].iloc[j,0], 'ok')
            
            axes5 = f.add_subplot(2,2,4, sharex = axes1)
            axes5.set_title(final_filenames[4], fontsize = 15, weight = 'bold')
            axes5.set_xlabel('Value Length', fontsize = 15, weight = 'bold')
            axes5.yaxis.set_ticklabels([])

            for j in range(self.consistDF[4].shape[0]):
                axes5.hlines(y = self.consistDF[4].iloc[j,0], xmin = self.consistDF[4].iloc[j,1] , xmax = self.consistDF[4].iloc[j,2], lw = 2, color = 'k')
                if self.consistDF[4].iloc[j,1] != self.consistDF[4].iloc[j,2]:
                    axes5.plot(self.consistDF[4].iloc[j,1], self.consistDF[4].iloc[j,0], '|k', ms = 15, mew = 1.5)
                    axes5.plot(self.consistDF[4].iloc[j,2], self.consistDF[4].iloc[j,0], '|k', ms = 15, mew = 1.5)
                else:
                    axes5.plot(self.consistDF[4].iloc[j,2], self.consistDF[4].iloc[j,0], 'ok')

        if counter == 6:
            axes1 = f.add_subplot(2,3,1)
            axes1.set_title(final_filenames[0], fontsize = 15, weight = 'bold')
            axes1.set_ylabel('Variables', fontsize = 15, weight = 'bold')
            axes1.set_xticks(ticks)

            for j in range(self.consistDF[0].shape[0]):
                axes1.hlines(y = self.consistDF[0].iloc[j,0], xmin = self.consistDF[0].iloc[j,1] , xmax = self.consistDF[0].iloc[j,2], lw = 2, color = 'k')
                if self.consistDF[0].iloc[j,1] != self.consistDF[0].iloc[j,2]:
                    axes1.plot(self.consistDF[0].iloc[j,1], self.consistDF[0].iloc[j,0], '|k', ms = 15, mew = 1.5)
                    axes1.plot(self.consistDF[0].iloc[j,2], self.consistDF[0].iloc[j,0], '|k', ms = 15, mew = 1.5)
                else:
                    axes1.plot(self.consistDF[0].iloc[j,2], self.consistDF[0].iloc[j,0], 'ok')
                
            axes2 = f.add_subplot(2,3,2, sharex = axes1)
            axes2.set_title(final_filenames[1], fontsize = 15, weight = 'bold')
            axes2.yaxis.set_ticklabels([])

            for j in range(self.consistDF[1].shape[0]):
                axes2.hlines(y = self.consistDF[1].iloc[j,0], xmin = self.consistDF[1].iloc[j,1] , xmax = self.consistDF[1].iloc[j,2], lw = 2, color = 'k')
                if self.consistDF[1].iloc[j,1] != self.consistDF[1].iloc[j,2]:
                    axes2.plot(self.consistDF[1].iloc[j,1], self.consistDF[1].iloc[j,0], '|k', ms = 15, mew = 1.5)
                    axes2.plot(self.consistDF[1].iloc[j,2], self.consistDF[1].iloc[j,0], '|k', ms = 15, mew = 1.5)
                else:
                    axes2.plot(self.consistDF[1].iloc[j,2], self.consistDF[1].iloc[j,0], 'ok')

            axes3 = f.add_subplot(2,3,3, sharex = axes1)
            axes3.set_title(final_filenames[2], fontsize = 15, weight = 'bold')
            axes3.yaxis.set_ticklabels([])
            
            for j in range(self.consistDF[2].shape[0]):
                axes3.hlines(y = self.consistDF[2].iloc[j,0], xmin = self.consistDF[2].iloc[j,1] , xmax = self.consistDF[2].iloc[j,2], lw = 2, color = 'k')
                if self.consistDF[2].iloc[j,1] != self.consistDF[2].iloc[j,2]:
                    axes3.plot(self.consistDF[2].iloc[j,1], self.consistDF[2].iloc[j,0], '|k', ms = 15, mew = 1.5)
                    axes3.plot(self.consistDF[2].iloc[j,2], self.consistDF[2].iloc[j,0], '|k', ms = 15, mew = 1.5)
                else:
                    axes3.plot(self.consistDF[2].iloc[j,2], self.consistDF[2].iloc[j,0], 'ok')
            
            axes4 = f.add_subplot(2,3,4, sharex = axes1)
            axes4.set_title(final_filenames[3], fontsize = 15, weight = 'bold')
            axes4.set_xlabel('Value Length', fontsize = 15, weight = 'bold')
            axes4.set_ylabel('Variables', fontsize = 15, weight = 'bold')

            for j in range(self.consistDF[3].shape[0]):
                axes4.hlines(y = self.consistDF[3].iloc[j,0], xmin = self.consistDF[3].iloc[j,1] , xmax = self.consistDF[3].iloc[j,2], lw = 2, color = 'k')
                if self.consistDF[3].iloc[j,1] != self.consistDF[3].iloc[j,2]:
                    axes4.plot(self.consistDF[3].iloc[j,1], self.consistDF[3].iloc[j,0], '|k', ms = 15, mew = 1.5)
                    axes4.plot(self.consistDF[3].iloc[j,2], self.consistDF[3].iloc[j,0], '|k', ms = 15, mew = 1.5)
                else:
                    axes4.plot(self.consistDF[3].iloc[j,2], self.consistDF[3].iloc[j,0], 'ok')
            
            axes5 = f.add_subplot(2,3,5, sharex = axes1)
            axes5.set_title(final_filenames[4], fontsize = 15, weight = 'bold')
            axes5.set_xlabel('Value Length', fontsize = 15, weight = 'bold')
            axes5.yaxis.set_ticklabels([])

            for j in range(self.consistDF[4].shape[0]):
                axes5.hlines(y = self.consistDF[4].iloc[j,0], xmin = self.consistDF[4].iloc[j,1] , xmax = self.consistDF[4].iloc[j,2], lw = 2, color = 'k')
                if self.consistDF[4].iloc[j,1] != self.consistDF[4].iloc[j,2]:
                    axes5.plot(self.consistDF[4].iloc[j,1], self.consistDF[4].iloc[j,0], '|k', ms = 15, mew = 1.5)
                    axes5.plot(self.consistDF[4].iloc[j,2], self.consistDF[4].iloc[j,0], '|k', ms = 15, mew = 1.5)
                else:
                    axes5.plot(self.consistDF[4].iloc[j,2], self.consistDF[4].iloc[j,0], 'ok')
            
            axes6 = f.add_subplot(2,3,6, sharex = axes1)
            axes6.set_title(final_filenames[5], fontsize = 15, weight = 'bold')
            axes6.set_xlabel('Value Length', fontsize = 15, weight = 'bold')
            axes6.yaxis.set_ticklabels([])

            for j in range(self.consistDF[5].shape[0]):
                axes6.hlines(y = self.consistDF[5].iloc[j,0], xmin = self.consistDF[5].iloc[j,1] , xmax = self.consistDF[5].iloc[j,2], lw = 2, color = 'k')
                if self.consistDF[5].iloc[j,1] != self.consistDF[5].iloc[j,2]:
                    axes6.plot(self.consistDF[5].iloc[j,1], self.consistDF[5].iloc[j,0], '|k', ms = 15, mew = 1.5)
                    axes6.plot(self.consistDF[5].iloc[j,2], self.consistDF[5].iloc[j,0], '|k', ms = 15, mew = 1.5)
                else:
                    axes6.plot(self.consistDF[5].iloc[j,2], self.consistDF[5].iloc[j,0], 'ok')
        
        if counter == 7:
            axes1 = f.add_subplot(3,3,1)
            axes1.set_title(final_filenames[0], fontsize = 15, weight = 'bold')
            axes1.set_xlabel('Value Length', fontsize = 15, weight = 'bold')
            axes1.set_ylabel('Variables', fontsize = 15, weight = 'bold')
            axes1.set_xticks(ticks)
            f.subplots_adjust(hspace = 0.5)

            for j in range(self.consistDF[0].shape[0]):
                axes1.hlines(y = self.consistDF[0].iloc[j,0], xmin = self.consistDF[0].iloc[j,1] , xmax = self.consistDF[0].iloc[j,2], lw = 2, color = 'k')
                if self.consistDF[0].iloc[j,1] != self.consistDF[0].iloc[j,2]:
                    axes1.plot(self.consistDF[0].iloc[j,1], self.consistDF[0].iloc[j,0], '|k', ms = 9, mew = 1.5)
                    axes1.plot(self.consistDF[0].iloc[j,2], self.consistDF[0].iloc[j,0], '|k', ms = 9, mew = 1.5)
                else:
                    axes1.plot(self.consistDF[0].iloc[j,2], self.consistDF[0].iloc[j,0], 'ok')
                
            axes2 = f.add_subplot(3,3,2, sharex = axes1)
            axes2.set_title(final_filenames[1], fontsize = 15, weight = 'bold')
            axes2.set_xlabel('Value Length', fontsize = 15, weight = 'bold')
            axes2.yaxis.set_ticklabels([])

            for j in range(self.consistDF[1].shape[0]):
                axes2.hlines(y = self.consistDF[1].iloc[j,0], xmin = self.consistDF[1].iloc[j,1] , xmax = self.consistDF[1].iloc[j,2], lw = 2, color = 'k')
                if self.consistDF[1].iloc[j,1] != self.consistDF[1].iloc[j,2]:
                    axes2.plot(self.consistDF[1].iloc[j,1], self.consistDF[1].iloc[j,0], '|k', ms = 9, mew = 1.5)
                    axes2.plot(self.consistDF[1].iloc[j,2], self.consistDF[1].iloc[j,0], '|k', ms = 9, mew = 1.5)
                else:
                    axes2.plot(self.consistDF[1].iloc[j,2], self.consistDF[1].iloc[j,0], 'ok')

            axes3 = f.add_subplot(3,3,3, sharex = axes1)
            axes3.set_title(final_filenames[2], fontsize = 15, weight = 'bold')
            axes3.set_xlabel('Value Length', fontsize = 15, weight = 'bold')
            axes3.yaxis.set_ticklabels([])
            
            for j in range(self.consistDF[2].shape[0]):
                axes3.hlines(y = self.consistDF[2].iloc[j,0], xmin = self.consistDF[2].iloc[j,1] , xmax = self.consistDF[2].iloc[j,2], lw = 2, color = 'k')
                if self.consistDF[2].iloc[j,1] != self.consistDF[2].iloc[j,2]:
                    axes3.plot(self.consistDF[2].iloc[j,1], self.consistDF[2].iloc[j,0], '|k', ms = 9, mew = 1.5)
                    axes3.plot(self.consistDF[2].iloc[j,2], self.consistDF[2].iloc[j,0], '|k', ms = 9, mew = 1.5)
                else:
                    axes3.plot(self.consistDF[2].iloc[j,2], self.consistDF[2].iloc[j,0], 'ok')
            
            axes4 = f.add_subplot(3,2,3, sharex = axes1)
            axes4.set_title(final_filenames[3], fontsize = 15, weight = 'bold')
            axes4.set_xlabel('Value Length', fontsize = 15, weight = 'bold')
            axes4.set_ylabel('Variables', fontsize = 15, weight = 'bold')

            for j in range(self.consistDF[3].shape[0]):
                axes4.hlines(y = self.consistDF[3].iloc[j,0], xmin = self.consistDF[3].iloc[j,1] , xmax = self.consistDF[3].iloc[j,2], lw = 2, color = 'k')
                if self.consistDF[3].iloc[j,1] != self.consistDF[3].iloc[j,2]:
                    axes4.plot(self.consistDF[3].iloc[j,1], self.consistDF[3].iloc[j,0], '|k', ms = 9, mew = 1.5)
                    axes4.plot(self.consistDF[3].iloc[j,2], self.consistDF[3].iloc[j,0], '|k', ms = 9, mew = 1.5)
                else:
                    axes4.plot(self.consistDF[3].iloc[j,2], self.consistDF[3].iloc[j,0], 'ok')
            
            axes5 = f.add_subplot(3,2,4, sharex = axes1)
            axes5.set_title(final_filenames[4], fontsize = 15, weight = 'bold')
            axes5.set_xlabel('Value Length', fontsize = 15, weight = 'bold')
            axes5.yaxis.set_ticklabels([])

            for j in range(self.consistDF[4].shape[0]):
                axes5.hlines(y = self.consistDF[4].iloc[j,0], xmin = self.consistDF[4].iloc[j,1] , xmax = self.consistDF[4].iloc[j,2], lw = 2, color = 'k')
                if self.consistDF[4].iloc[j,1] != self.consistDF[4].iloc[j,2]:
                    axes5.plot(self.consistDF[4].iloc[j,1], self.consistDF[4].iloc[j,0], '|k', ms = 9, mew = 1.5)
                    axes5.plot(self.consistDF[4].iloc[j,2], self.consistDF[4].iloc[j,0], '|k', ms = 9, mew = 1.5)
                else:
                    axes5.plot(self.consistDF[4].iloc[j,2], self.consistDF[4].iloc[j,0], 'ok')
            
            axes6 = f.add_subplot(3,2,5, sharex = axes1)
            axes6.set_title(final_filenames[5], fontsize = 15, weight = 'bold')
            axes6.set_xlabel('Value Length', fontsize = 15, weight = 'bold')
            axes6.set_ylabel('Variables', fontsize = 15, weight = 'bold')

            for j in range(self.consistDF[5].shape[0]):
                axes6.hlines(y = self.consistDF[5].iloc[j,0], xmin = self.consistDF[5].iloc[j,1] , xmax = self.consistDF[5].iloc[j,2], lw = 2, color = 'k')
                if self.consistDF[5].iloc[j,1] != self.consistDF[5].iloc[j,2]:
                    axes6.plot(self.consistDF[5].iloc[j,1], self.consistDF[5].iloc[j,0], '|k', ms = 9, mew = 1.5)
                    axes6.plot(self.consistDF[5].iloc[j,2], self.consistDF[5].iloc[j,0], '|k', ms = 9, mew = 1.5)
                else:
                    axes6.plot(self.consistDF[5].iloc[j,2], self.consistDF[5].iloc[j,0], 'ok')
            
            axes7 = f.add_subplot(3,2,6, sharex = axes1)
            axes7.set_title(final_filenames[6], fontsize = 15, weight = 'bold')
            axes7.set_xlabel('Value Length', fontsize = 15, weight = 'bold')
            axes7.yaxis.set_ticklabels([])

            for j in range(self.consistDF[6].shape[0]):
                axes7.hlines(y = self.consistDF[6].iloc[j,0], xmin = self.consistDF[6].iloc[j,1] , xmax = self.consistDF[6].iloc[j,2], lw = 2, color = 'k')
                if self.consistDF[6].iloc[j,1] != self.consistDF[6].iloc[j,2]:
                    axes7.plot(self.consistDF[6].iloc[j,1], self.consistDF[6].iloc[j,0], '|k', ms = 9, mew = 1.5)
                    axes7.plot(self.consistDF[6].iloc[j,2], self.consistDF[6].iloc[j,0], '|k', ms = 9, mew = 1.5)
                else:
                    axes7.plot(self.consistDF[6].iloc[j,2], self.consistDF[6].iloc[j,0], 'ok')

        if counter == 8:
            axes1 = f.add_subplot(3,3,1)
            axes1.set_title(final_filenames[0], fontsize = 15, weight = 'bold')
            axes1.set_xlabel('Value Length', fontsize = 15, weight = 'bold')
            axes1.set_ylabel('Variables', fontsize = 15, weight = 'bold')
            axes1.set_xticks(ticks)
            f.subplots_adjust(hspace = 0.6)

            for j in range(self.consistDF[0].shape[0]):
                axes1.hlines(y = self.consistDF[0].iloc[j,0], xmin = self.consistDF[0].iloc[j,1] , xmax = self.consistDF[0].iloc[j,2], lw = 2, color = 'k')
                if self.consistDF[0].iloc[j,1] != self.consistDF[0].iloc[j,2]:
                    axes1.plot(self.consistDF[0].iloc[j,1], self.consistDF[0].iloc[j,0], '|k', ms = 9, mew = 1.5)
                    axes1.plot(self.consistDF[0].iloc[j,2], self.consistDF[0].iloc[j,0], '|k', ms = 9, mew = 1.5)
                else:
                    axes1.plot(self.consistDF[0].iloc[j,2], self.consistDF[0].iloc[j,0], 'ok')
                
            axes2 = f.add_subplot(3,3,2, sharex = axes1)
            axes2.set_title(final_filenames[1], fontsize = 15, weight = 'bold')
            axes2.set_xlabel('Value Length', fontsize = 15, weight = 'bold')
            axes2.yaxis.set_ticklabels([])

            for j in range(self.consistDF[1].shape[0]):
                axes2.hlines(y = self.consistDF[1].iloc[j,0], xmin = self.consistDF[1].iloc[j,1] , xmax = self.consistDF[1].iloc[j,2], lw = 2, color = 'k')
                if self.consistDF[1].iloc[j,1] != self.consistDF[1].iloc[j,2]:
                    axes2.plot(self.consistDF[1].iloc[j,1], self.consistDF[1].iloc[j,0], '|k', ms = 9, mew = 1.5)
                    axes2.plot(self.consistDF[1].iloc[j,2], self.consistDF[1].iloc[j,0], '|k', ms = 9, mew = 1.5)
                else:
                    axes2.plot(self.consistDF[1].iloc[j,2], self.consistDF[1].iloc[j,0], 'ok')

            axes3 = f.add_subplot(3,3,3, sharex = axes1)
            axes3.set_title(final_filenames[2], fontsize = 15, weight = 'bold')
            axes3.set_xlabel('Value Length', fontsize = 15, weight = 'bold')
            axes3.yaxis.set_ticklabels([])
            
            for j in range(self.consistDF[2].shape[0]):
                axes3.hlines(y = self.consistDF[2].iloc[j,0], xmin = self.consistDF[2].iloc[j,1] , xmax = self.consistDF[2].iloc[j,2], lw = 2, color = 'k')
                if self.consistDF[2].iloc[j,1] != self.consistDF[2].iloc[j,2]:
                    axes3.plot(self.consistDF[2].iloc[j,1], self.consistDF[2].iloc[j,0], '|k', ms = 9, mew = 1.5)
                    axes3.plot(self.consistDF[2].iloc[j,2], self.consistDF[2].iloc[j,0], '|k', ms = 9, mew = 1.5)
                else:
                    axes3.plot(self.consistDF[2].iloc[j,2], self.consistDF[2].iloc[j,0], 'ok')
            
            axes4 = f.add_subplot(3,3,4, sharex = axes1)
            axes4.set_title(final_filenames[3], fontsize = 15, weight = 'bold')
            axes4.set_xlabel('Value Length', fontsize = 15, weight = 'bold')
            axes4.set_ylabel('Variables', fontsize = 15, weight = 'bold')

            for j in range(self.consistDF[3].shape[0]):
                axes4.hlines(y = self.consistDF[3].iloc[j,0], xmin = self.consistDF[3].iloc[j,1] , xmax = self.consistDF[3].iloc[j,2], lw = 2, color = 'k')
                if self.consistDF[3].iloc[j,1] != self.consistDF[3].iloc[j,2]:
                    axes4.plot(self.consistDF[3].iloc[j,1], self.consistDF[3].iloc[j,0], '|k', ms = 9, mew = 1.5)
                    axes4.plot(self.consistDF[3].iloc[j,2], self.consistDF[3].iloc[j,0], '|k', ms = 9, mew = 1.5)
                else:
                    axes4.plot(self.consistDF[3].iloc[j,2], self.consistDF[3].iloc[j,0], 'ok')
            
            axes5 = f.add_subplot(3,3,5, sharex = axes1)
            axes5.set_title(final_filenames[4], fontsize = 15, weight = 'bold')
            axes5.set_xlabel('Value Length', fontsize = 15, weight = 'bold')
            axes5.yaxis.set_ticklabels([])

            for j in range(self.consistDF[4].shape[0]):
                axes5.hlines(y = self.consistDF[4].iloc[j,0], xmin = self.consistDF[4].iloc[j,1] , xmax = self.consistDF[4].iloc[j,2], lw = 2, color = 'k')
                if self.consistDF[4].iloc[j,1] != self.consistDF[4].iloc[j,2]:
                    axes5.plot(self.consistDF[4].iloc[j,1], self.consistDF[4].iloc[j,0], '|k', ms = 9, mew = 1.5)
                    axes5.plot(self.consistDF[4].iloc[j,2], self.consistDF[4].iloc[j,0], '|k', ms = 9, mew = 1.5)
                else:
                    axes5.plot(self.consistDF[4].iloc[j,2], self.consistDF[4].iloc[j,0], 'ok')
            
            axes6 = f.add_subplot(3,3,6, sharex = axes1)
            axes6.set_title(final_filenames[5], fontsize = 15, weight = 'bold')
            axes6.set_xlabel('Value Length', fontsize = 15, weight = 'bold')
            axes6.yaxis.set_ticklabels([])

            for j in range(self.consistDF[5].shape[0]):
                axes6.hlines(y = self.consistDF[5].iloc[j,0], xmin = self.consistDF[5].iloc[j,1] , xmax = self.consistDF[5].iloc[j,2], lw = 2, color = 'k')
                if self.consistDF[5].iloc[j,1] != self.consistDF[5].iloc[j,2]:
                    axes6.plot(self.consistDF[5].iloc[j,1], self.consistDF[5].iloc[j,0], '|k', ms = 9, mew = 1.5)
                    axes6.plot(self.consistDF[5].iloc[j,2], self.consistDF[5].iloc[j,0], '|k', ms = 9, mew = 1.5)
                else:
                    axes6.plot(self.consistDF[5].iloc[j,2], self.consistDF[5].iloc[j,0], 'ok')
            
            axes7 = f.add_subplot(3,2,5, sharex = axes1)
            axes7.set_title(final_filenames[6], fontsize = 15, weight = 'bold')
            axes7.set_xlabel('Value Length', fontsize = 15, weight = 'bold')
            axes7.set_ylabel('Variables', fontsize = 15, weight = 'bold')

            for j in range(self.consistDF[6].shape[0]):
                axes7.hlines(y = self.consistDF[6].iloc[j,0], xmin = self.consistDF[6].iloc[j,1] , xmax = self.consistDF[6].iloc[j,2], lw = 2, color = 'k')
                if self.consistDF[6].iloc[j,1] != self.consistDF[6].iloc[j,2]:
                    axes7.plot(self.consistDF[6].iloc[j,1], self.consistDF[6].iloc[j,0], '|k', ms = 9, mew = 1.5)
                    axes7.plot(self.consistDF[6].iloc[j,2], self.consistDF[6].iloc[j,0], '|k', ms = 9, mew = 1.5)
                else:
                    axes7.plot(self.consistDF[6].iloc[j,2], self.consistDF[6].iloc[j,0], 'ok')
            
            axes8 = f.add_subplot(3,2,6, sharex = axes1)
            axes8.set_title(final_filenames[7], fontsize = 15, weight = 'bold')
            axes8.set_xlabel('Value Length', fontsize = 15, weight = 'bold')
            axes8.yaxis.set_ticklabels([])

            for j in range(self.consistDF[7].shape[0]):
                axes8.hlines(y = self.consistDF[7].iloc[j,0], xmin = self.consistDF[7].iloc[j,1] , xmax = self.consistDF[7].iloc[j,2], lw = 2, color = 'k')
                if self.consistDF[7].iloc[j,1] != self.consistDF[7].iloc[j,2]:
                    axes8.plot(self.consistDF[7].iloc[j,1], self.consistDF[7].iloc[j,0], '|k', ms = 9, mew = 1.5)
                    axes8.plot(self.consistDF[7].iloc[j,2], self.consistDF[7].iloc[j,0], '|k', ms = 9, mew = 1.5)
                else:
                    axes8.plot(self.consistDF[7].iloc[j,2], self.consistDF[7].iloc[j,0], 'ok')

        if counter == 9:
            axes1 = f.add_subplot(3,3,1)
            axes1.set_title(final_filenames[0], fontsize = 15, weight = 'bold')
            axes1.set_ylabel('Variables', fontsize = 15, weight = 'bold')
            axes1.set_xticks(ticks)
            f.subplots_adjust(hspace = 0.4)

            for j in range(self.consistDF[0].shape[0]):
                axes1.hlines(y = self.consistDF[0].iloc[j,0], xmin = self.consistDF[0].iloc[j,1] , xmax = self.consistDF[0].iloc[j,2], lw = 2, color = 'k')
                if self.consistDF[0].iloc[j,1] != self.consistDF[0].iloc[j,2]:
                    axes1.plot(self.consistDF[0].iloc[j,1], self.consistDF[0].iloc[j,0], '|k', ms = 9, mew = 1.5)
                    axes1.plot(self.consistDF[0].iloc[j,2], self.consistDF[0].iloc[j,0], '|k', ms = 9, mew = 1.5)
                else:
                    axes1.plot(self.consistDF[0].iloc[j,2], self.consistDF[0].iloc[j,0], 'ok')
                
            axes2 = f.add_subplot(3,3,2, sharex = axes1)
            axes2.set_title(final_filenames[1], fontsize = 15, weight = 'bold')
            axes2.yaxis.set_ticklabels([])

            for j in range(self.consistDF[1].shape[0]):
                axes2.hlines(y = self.consistDF[1].iloc[j,0], xmin = self.consistDF[1].iloc[j,1] , xmax = self.consistDF[1].iloc[j,2], lw = 2, color = 'k')
                if self.consistDF[1].iloc[j,1] != self.consistDF[1].iloc[j,2]:
                    axes2.plot(self.consistDF[1].iloc[j,1], self.consistDF[1].iloc[j,0], '|k', ms = 9, mew = 1.5)
                    axes2.plot(self.consistDF[1].iloc[j,2], self.consistDF[1].iloc[j,0], '|k', ms = 9, mew = 1.5)
                else:
                    axes2.plot(self.consistDF[1].iloc[j,2], self.consistDF[1].iloc[j,0], 'ok')

            axes3 = f.add_subplot(3,3,3, sharex = axes1)
            axes3.set_title(final_filenames[2], fontsize = 15, weight = 'bold')
            axes3.yaxis.set_ticklabels([])
            
            for j in range(self.consistDF[2].shape[0]):
                axes3.hlines(y = self.consistDF[2].iloc[j,0], xmin = self.consistDF[2].iloc[j,1] , xmax = self.consistDF[2].iloc[j,2], lw = 2, color = 'k')
                if self.consistDF[2].iloc[j,1] != self.consistDF[2].iloc[j,2]:
                    axes3.plot(self.consistDF[2].iloc[j,1], self.consistDF[2].iloc[j,0], '|k', ms = 9, mew = 1.5)
                    axes3.plot(self.consistDF[2].iloc[j,2], self.consistDF[2].iloc[j,0], '|k', ms = 9, mew = 1.5)
                else:
                    axes3.plot(self.consistDF[2].iloc[j,2], self.consistDF[2].iloc[j,0], 'ok')
            
            axes4 = f.add_subplot(3,3,4, sharex = axes1)
            axes4.set_title(final_filenames[3], fontsize = 15, weight = 'bold')
            axes4.set_ylabel('Variables', fontsize = 15, weight = 'bold')

            for j in range(self.consistDF[3].shape[0]):
                axes4.hlines(y = self.consistDF[3].iloc[j,0], xmin = self.consistDF[3].iloc[j,1] , xmax = self.consistDF[3].iloc[j,2], lw = 2, color = 'k')
                if self.consistDF[3].iloc[j,1] != self.consistDF[3].iloc[j,2]:
                    axes4.plot(self.consistDF[3].iloc[j,1], self.consistDF[3].iloc[j,0], '|k', ms = 9, mew = 1.5)
                    axes4.plot(self.consistDF[3].iloc[j,2], self.consistDF[3].iloc[j,0], '|k', ms = 9, mew = 1.5)
                else:
                    axes4.plot(self.consistDF[3].iloc[j,2], self.consistDF[3].iloc[j,0], 'ok')
            
            axes5 = f.add_subplot(3,3,5, sharex = axes1)
            axes5.set_title(final_filenames[4], fontsize = 15, weight = 'bold')
            axes5.yaxis.set_ticklabels([])

            for j in range(self.consistDF[4].shape[0]):
                axes5.hlines(y = self.consistDF[4].iloc[j,0], xmin = self.consistDF[4].iloc[j,1] , xmax = self.consistDF[4].iloc[j,2], lw = 2, color = 'k')
                if self.consistDF[4].iloc[j,1] != self.consistDF[4].iloc[j,2]:
                    axes5.plot(self.consistDF[4].iloc[j,1], self.consistDF[4].iloc[j,0], '|k', ms = 9, mew = 1.5)
                    axes5.plot(self.consistDF[4].iloc[j,2], self.consistDF[4].iloc[j,0], '|k', ms = 9, mew = 1.5)
                else:
                    axes5.plot(self.consistDF[4].iloc[j,2], self.consistDF[4].iloc[j,0], 'ok')
            
            axes6 = f.add_subplot(3,3,6, sharex = axes1)
            axes6.set_title(final_filenames[5], fontsize = 15, weight = 'bold')
            axes6.yaxis.set_ticklabels([])

            for j in range(self.consistDF[5].shape[0]):
                axes6.hlines(y = self.consistDF[5].iloc[j,0], xmin = self.consistDF[5].iloc[j,1] , xmax = self.consistDF[5].iloc[j,2], lw = 2, color = 'k')
                if self.consistDF[5].iloc[j,1] != self.consistDF[5].iloc[j,2]:
                    axes6.plot(self.consistDF[5].iloc[j,1], self.consistDF[5].iloc[j,0], '|k', ms = 9, mew = 1.5)
                    axes6.plot(self.consistDF[5].iloc[j,2], self.consistDF[5].iloc[j,0], '|k', ms = 9, mew = 1.5)
                else:
                    axes6.plot(self.consistDF[5].iloc[j,2], self.consistDF[5].iloc[j,0], 'ok')
            
            axes7 = f.add_subplot(3,3,7, sharex = axes1)
            axes7.set_title(final_filenames[6], fontsize = 15, weight = 'bold')
            axes7.set_xlabel('Value Length', fontsize = 15, weight = 'bold')
            axes7.set_ylabel('Variables', fontsize = 15, weight = 'bold')

            for j in range(self.consistDF[6].shape[0]):
                axes7.hlines(y = self.consistDF[6].iloc[j,0], xmin = self.consistDF[6].iloc[j,1] , xmax = self.consistDF[6].iloc[j,2], lw = 2, color = 'k')
                if self.consistDF[6].iloc[j,1] != self.consistDF[6].iloc[j,2]:
                    axes7.plot(self.consistDF[6].iloc[j,1], self.consistDF[6].iloc[j,0], '|k', ms = 9, mew = 1.5)
                    axes7.plot(self.consistDF[6].iloc[j,2], self.consistDF[6].iloc[j,0], '|k', ms = 9, mew = 1.5)
                else:
                    axes7.plot(self.consistDF[6].iloc[j,2], self.consistDF[6].iloc[j,0], 'ok')
            
            axes8 = f.add_subplot(3,3,8, sharex = axes1)
            axes8.set_title(final_filenames[7], fontsize = 15, weight = 'bold')
            axes8.set_xlabel('Value Length', fontsize = 15, weight = 'bold')
            axes8.yaxis.set_ticklabels([])
            

            for j in range(self.consistDF[7].shape[0]):
                axes8.hlines(y = self.consistDF[7].iloc[j,0], xmin = self.consistDF[7].iloc[j,1] , xmax = self.consistDF[7].iloc[j,2], lw = 2, color = 'k')
                if self.consistDF[7].iloc[j,1] != self.consistDF[7].iloc[j,2]:
                    axes8.plot(self.consistDF[7].iloc[j,1], self.consistDF[7].iloc[j,0], '|k', ms = 9, mew = 1.5)
                    axes8.plot(self.consistDF[7].iloc[j,2], self.consistDF[7].iloc[j,0], '|k', ms = 9, mew = 1.5)
                else:
                    axes8.plot(self.consistDF[7].iloc[j,2], self.consistDF[7].iloc[j,0], 'ok')
            
            axes9 = f.add_subplot(3,3,9, sharex = axes1)
            axes9.set_title(final_filenames[8], fontsize = 15, weight = 'bold')
            axes9.set_xlabel('Value Length', fontsize = 15, weight = 'bold')
            axes9.yaxis.set_ticklabels([])

            for j in range(self.consistDF[8].shape[0]):
                axes9.hlines(y = self.consistDF[8].iloc[j,0], xmin = self.consistDF[8].iloc[j,1] , xmax = self.consistDF[8].iloc[j,2], lw = 2, color = 'k')
                if self.consistDF[8].iloc[j,1] != self.consistDF[8].iloc[j,2]:
                    axes9.plot(self.consistDF[8].iloc[j,1], self.consistDF[8].iloc[j,0], '|k', ms = 9, mew = 1.5)
                    axes9.plot(self.consistDF[8].iloc[j,2], self.consistDF[8].iloc[j,0], '|k', ms = 9, mew = 1.5)
                else:
                    axes9.plot(self.consistDF[8].iloc[j,2], self.consistDF[8].iloc[j,0], 'ok')

########################################################### COMPLETENESS ######################################################################
class Completeness(): 

        completeDF = []

        def calMissingvalues(self, df): 
            """Calculate percentage of missing data"""
            self.mvTable = df.isna().mean().round(4) * 100 # Find And Round Up Average Of Total Number Of Missing Data
            self.xVariable = list(df.columns.values.tolist()) # Create A List For Column Names
            self.val = list(self.mvTable.to_numpy()) # Create A List For Percentage Of Missing Data 
            my_dict = {'Variables':self.xVariable, 'Percentage of missing value (%)':self.val}
            self.df = pd.DataFrame(data = my_dict)
            self.completeDF.append(self.df) # Add Dataframe Into A List

        def plotBarh(self,window):
            """Plot horizontal bar and dot charts onto the new window"""
            counter = len(self.completeDF)
            f = plt.Figure(figsize = (10,20))
            canvas = FigureCanvasTkAgg(f, window)
            canvas.get_tk_widget().pack(side = tk.TOP, fill = tk.BOTH, expand = True)

################################################### HORIZONTAL BAR AND DOT CHART PLOTS ########################################################
            if counter == 1:
                axes = f.add_subplot(1,1,1)
                axes.set_title(final_filenames[0], fontsize = 15, weight = 'bold')
                axes.set_xlabel('Percentage of missing value (%)', fontsize = 15, weight = 'bold')
                axes.set_ylabel('Variables', fontsize = 15, weight = 'bold')

                self.completeDF[0].plot(kind = 'barh',
                            legend = False,
                            ax = axes,
                            x = 'Variables',
                            y = 'Percentage of missing value (%)',
                            xlim = (0,100))  
    
                for j in range(self.completeDF[0].shape[0]):
                    if self.completeDF[0].iloc[j,1] == 0:
                        axes.plot(self.completeDF[0].iloc[j,1], self.completeDF[0].iloc[j,0], 'ok', ms = 10)
                    else:
                        axes.plot(self.completeDF[0].iloc[j,1], self.completeDF[0].iloc[j,0], 'ok', ms = 0)
                        
            elif counter == 2:
                
                axes1 = f.add_subplot(1,2,1)
                axes1.set_title(final_filenames[0], fontsize = 15, weight = 'bold')
                axes1.set_xlabel('Percentage of missing value (%)', fontsize = 15, weight = 'bold')
                axes1.set_ylabel('Variables', fontsize = 15, weight = 'bold')
                self.completeDF[0].plot(kind = 'barh',
                            legend = False,
                            ax = axes1,
                            x = 'Variables',
                            y = 'Percentage of missing value (%)',
                            xlim = (0,100))
                
                for j in range(self.completeDF[0].shape[0]):
                    if self.completeDF[0].iloc[j,1] == 0:
                        axes1.plot(self.completeDF[0].iloc[j,1], self.completeDF[0].iloc[j,0], 'ok', ms = 10)
                    else:
                        axes1.plot(self.completeDF[0].iloc[j,1], self.completeDF[0].iloc[j,0], 'ok', ms = 0)

                axes2 = f.add_subplot(1,2,2, sharey = axes1)
                axes2.set_title(final_filenames[1], fontsize = 15, weight = 'bold')
                axes2.set_xlabel('Percentage of missing value (%)', fontsize = 15, weight = 'bold')
                self.completeDF[1].plot(kind = 'barh',
                            legend = False,
                            ax = axes2,
                            x = 'Variables',
                            y = 'Percentage of missing value (%)',
                            xlim = (0,100))
                
                for j in range(self.completeDF[1].shape[0]):
                    if self.completeDF[1].iloc[j,1] == 0:
                        axes2.plot(self.completeDF[1].iloc[j,1], self.completeDF[1].iloc[j,0], 'ok', ms = 10)
                    else:
                        axes2.plot(self.completeDF[1].iloc[j,1], self.completeDF[1].iloc[j,0], 'ok', ms = 0)

            elif counter == 3:

                axes1 = f.add_subplot(1,3,1)
                axes1.set_title(final_filenames[0], fontsize = 15, weight = 'bold')
                axes1.set_xlabel('Percentage of missing value (%)', fontsize = 15, weight = 'bold')
                axes1.set_ylabel('Variables', fontsize = 15, weight = 'bold')
                self.completeDF[0].plot(kind = 'barh',
                            legend = False,
                            ax = axes1,
                            x = 'Variables',
                            y = 'Percentage of missing value (%)',
                            xlim = (0,100))

                for j in range(self.completeDF[0].shape[0]):
                    if self.completeDF[0].iloc[j,1] == 0:
                        axes1.plot(self.completeDF[0].iloc[j,1], self.completeDF[0].iloc[j,0], 'ok', ms = 10)
                    else:
                        axes1.plot(self.completeDF[0].iloc[j,1], self.completeDF[0].iloc[j,0], 'ok', ms = 0)

                axes2 = f.add_subplot(1,3,2, sharey = axes1)
                axes2.set_title(final_filenames[1], fontsize = 15, weight = 'bold')
                axes2.set_xlabel('Percentage of missing value (%)', fontsize = 15, weight = 'bold')
                self.completeDF[1].plot(kind = 'barh',
                            legend = False,
                            ax = axes2,
                            x = 'Variables',
                            y = 'Percentage of missing value (%)',
                            xlim = (0,100))
                
                for j in range(self.completeDF[1].shape[0]):
                    if self.completeDF[1].iloc[j,1] == 0:
                        axes2.plot(self.completeDF[1].iloc[j,1], self.completeDF[1].iloc[j,0], 'ok', ms = 10)
                    else:
                        axes2.plot(self.completeDF[1].iloc[j,1], self.completeDF[1].iloc[j,0], 'ok', ms = 0)

                axes3 = f.add_subplot(1,3,3, sharey = axes1)
                axes3.set_title(final_filenames[2], fontsize = 15, weight = 'bold')
                axes3.set_xlabel('Percentage of missing value (%)', fontsize = 15, weight = 'bold')
                self.completeDF[2].plot(kind = 'barh',
                            legend = False,
                            ax = axes3,
                            x = 'Variables',
                            y = 'Percentage of missing value (%)',
                            xlim = (0,100))  

                for j in range(self.completeDF[2].shape[0]):
                    if self.completeDF[2].iloc[j,1] == 0:
                        axes3.plot(self.completeDF[2].iloc[j,1], self.completeDF[2].iloc[j,0], 'ok', ms = 10)
                    else:
                        axes3.plot(self.completeDF[2].iloc[j,1], self.completeDF[2].iloc[j,0], 'ok', ms = 0)            

            elif counter == 4:
                axes1 = f.add_subplot(2,2,1)
                axes1.set_title(final_filenames[0], fontsize = 15, weight = 'bold')
                axes1.set_ylabel('Variables', fontsize = 15, weight = 'bold')
                self.completeDF[0].plot(kind = 'barh',
                            legend = False,
                            ax = axes1,
                            x = 'Variables',
                            y = 'Percentage of missing value (%)',
                            xlim = (0,100))

                for j in range(self.completeDF[0].shape[0]):
                    if self.completeDF[0].iloc[j,1] == 0:
                        axes1.plot(self.completeDF[0].iloc[j,1], self.completeDF[0].iloc[j,0], 'ok', ms = 10)
                    else:
                        axes1.plot(self.completeDF[0].iloc[j,1], self.completeDF[0].iloc[j,0], 'ok', ms = 0)

                axes2 = f.add_subplot(2,2,2, sharey = axes1)
                axes2.set_title(final_filenames[1], fontsize = 15, weight = 'bold')
                self.completeDF[1].plot(kind = 'barh',
                            legend = False,
                            ax = axes2,
                            x = 'Variables',
                            y = 'Percentage of missing value (%)',
                            xlim = (0,100))
                
                for j in range(self.completeDF[1].shape[0]):
                    if self.completeDF[1].iloc[j,1] == 0:
                        axes2.plot(self.completeDF[1].iloc[j,1], self.completeDF[1].iloc[j,0], 'ok', ms = 10)
                    else:
                        axes2.plot(self.completeDF[1].iloc[j,1], self.completeDF[1].iloc[j,0], 'ok', ms = 0)
                
                axes3 = f.add_subplot(2,2,3)
                axes3.set_title(final_filenames[2], fontsize = 15, weight = 'bold')
                axes3.set_ylabel('Variables', fontsize =15, weight = 'bold')
                axes3.set_xlabel('Percentage of missing value (%)', fontsize =15, weight = 'bold')
                self.completeDF[2].plot(kind = 'barh',
                            legend = False,
                            ax = axes3,
                            x = 'Variables',
                            y = 'Percentage of missing value (%)',
                            xlim = (0,100))
                
                for j in range(self.completeDF[2].shape[0]):
                    if self.completeDF[2].iloc[j,1] == 0:
                        axes3.plot(self.completeDF[2].iloc[j,1], self.completeDF[2].iloc[j,0], 'ok', ms = 10)
                    else:
                        axes3.plot(self.completeDF[2].iloc[j,1], self.completeDF[2].iloc[j,0], 'ok', ms = 0)
                            
                axes4 = f.add_subplot(2,2,4, sharey = axes3)
                axes4.set_title(final_filenames[3], fontsize = 15, weight = 'bold')
                axes4.set_xlabel('Percentage of missing value (%)', fontsize = 15, weight = 'bold')
                self.completeDF[3].plot(kind = 'barh',
                            legend = False,
                            ax = axes4,
                            x = 'Variables',
                            y = 'Percentage of missing value (%)',
                            xlim = (0,100))

                for j in range(self.completeDF[3].shape[0]):
                    if self.completeDF[3].iloc[j,1] == 0:
                        axes4.plot(self.completeDF[3].iloc[j,1], self.completeDF[3].iloc[j,0], 'ok', ms = 10)
                    else:
                        axes4.plot(self.completeDF[3].iloc[j,1], self.completeDF[3].iloc[j,0], 'ok', ms = 0)   

            elif counter == 5:
                axes1 = f.add_subplot(2,3,1)
                axes1.set_title(final_filenames[0], fontsize = 15, weight = 'bold')
                axes1.set_xlabel('Percentage of missing value (%)', fontsize = 15, weight = 'bold')
                axes1.set_ylabel('Variables', fontsize = 15, weight = 'bold')
                f.subplots_adjust(hspace = 0.3)
                self.completeDF[0].plot(kind = 'barh',
                            legend = False,
                            ax = axes1,
                            x = 'Variables',
                            y = 'Percentage of missing value (%)',
                            xlim = (0,100))

                for j in range(self.completeDF[0].shape[0]):
                    if self.completeDF[0].iloc[j,1] == 0:
                        axes1.plot(self.completeDF[0].iloc[j,1], self.completeDF[0].iloc[j,0], 'ok', ms = 10)
                    else:
                        axes1.plot(self.completeDF[0].iloc[j,1], self.completeDF[0].iloc[j,0], 'ok', ms = 0)

                axes2 = f.add_subplot(2,3,2, sharey = axes1)
                axes2.set_title(final_filenames[1], fontsize = 15, weight = 'bold')
                axes2.set_xlabel('Percentage of missing value (%)', fontsize = 15, weight = 'bold')
                self.completeDF[1].plot(kind = 'barh',
                            legend = False,
                            ax = axes2,
                            x = 'Variables',
                            y = 'Percentage of missing value (%)',
                            xlim = (0,100))

                for j in range(self.completeDF[1].shape[0]):
                    if self.completeDF[1].iloc[j,1] == 0:
                        axes2.plot(self.completeDF[1].iloc[j,1], self.completeDF[1].iloc[j,0], 'ok', ms = 10)
                    else:
                        axes2.plot(self.completeDF[1].iloc[j,1], self.completeDF[1].iloc[j,0], 'ok', ms = 0)
                
                axes3 = f.add_subplot(2,3,3, sharey = axes1)
                axes3.set_title(final_filenames[2], fontsize = 15, weight = 'bold')
                axes3.set_xlabel('Percentage of missing value (%)', fontsize =15, weight = 'bold')
                self.completeDF[2].plot(kind = 'barh',
                            legend = False,
                            ax = axes3,
                            x = 'Variables',
                            y = 'Percentage of missing value (%)',
                            xlim = (0,100))
                
                for j in range(self.completeDF[2].shape[0]):
                    if self.completeDF[2].iloc[j,1] == 0:
                        axes3.plot(self.completeDF[2].iloc[j,1], self.completeDF[2].iloc[j,0], 'ok', ms = 10)
                    else:
                        axes3.plot(self.completeDF[2].iloc[j,1], self.completeDF[2].iloc[j,0], 'ok', ms = 0)
                     
                axes4 = f.add_subplot(2,2,3)
                axes4.set_title(final_filenames[3], fontsize = 15, weight = 'bold')
                axes4.set_xlabel('Percentage of missing value (%)', fontsize = 15, weight = 'bold')
                axes4.set_ylabel('Variables', fontsize = 15, weight = 'bold')
                self.completeDF[3].plot(kind = 'barh',
                            legend = False,
                            ax = axes4,
                            x = 'Variables',
                            y = 'Percentage of missing value (%)',
                            xlim = (0,100))
                
                for j in range(self.completeDF[3].shape[0]):
                    if self.completeDF[3].iloc[j,1] == 0:
                        axes4.plot(self.completeDF[3].iloc[j,1], self.completeDF[3].iloc[j,0], 'ok', ms = 10)
                    else:
                        axes4.plot(self.completeDF[3].iloc[j,1], self.completeDF[3].iloc[j,0], 'ok', ms = 0)

                axes5 = f.add_subplot(2,2,4, sharey = axes4)
                axes5.set_title(final_filenames[4], fontsize = 15, weight = 'bold')
                axes5.set_xlabel('Percentage of missing value (%)', fontsize = 15, weight = 'bold')
                self.completeDF[4].plot(kind = 'barh',
                            legend = False,
                            ax = axes5,
                            x = 'Variables',
                            y = 'Percentage of missing value (%)',
                            xlim = (0,100))

                for j in range(self.completeDF[4].shape[0]):
                    if self.completeDF[4].iloc[j,1] == 0:
                        axes5.plot(self.completeDF[4].iloc[j,1], self.completeDF[4].iloc[j,0], 'ok', ms = 10)
                    else:
                        axes5.plot(self.completeDF[4].iloc[j,1], self.completeDF[4].iloc[j,0], 'ok', ms = 0)

            elif counter == 6:
            
                axes1 = f.add_subplot(2,3,1)
                axes1.set_title(final_filenames[0], fontsize = 15, weight = 'bold')
                axes1.set_ylabel('Variables', fontsize = 15, weight = 'bold')
                f.subplots_adjust(hspace=0.3)
                self.completeDF[0].plot(kind = 'barh',
                            legend = False,
                            ax = axes1,
                            x = 'Variables',
                            y = 'Percentage of missing value (%)',
                            xlim = (0,100))
                
                for j in range(self.completeDF[0].shape[0]):
                    if self.completeDF[0].iloc[j,1] == 0:
                        axes1.plot(self.completeDF[0].iloc[j,1], self.completeDF[0].iloc[j,0], 'ok', ms = 10)
                    else:
                        axes1.plot(self.completeDF[0].iloc[j,1], self.completeDF[0].iloc[j,0], 'ok', ms = 0)

                axes2 = f.add_subplot(2,3,2, sharey = axes1)
                axes2.set_title(final_filenames[1], fontsize = 15, weight = 'bold')
                self.completeDF[1].plot(kind = 'barh',
                            legend = False,
                            ax = axes2,
                            x = 'Variables',
                            y = 'Percentage of missing value (%)',
                            xlim = (0,100))
                
                for j in range(self.completeDF[1].shape[0]):
                    if self.completeDF[1].iloc[j,1] == 0:
                        axes2.plot(self.completeDF[1].iloc[j,1], self.completeDF[1].iloc[j,0], 'ok', ms = 10)
                    else:
                        axes2.plot(self.completeDF[1].iloc[j,1], self.completeDF[1].iloc[j,0], 'ok', ms = 0)

                axes3 = f.add_subplot(2,3,3, sharey = axes1)
                axes3.set_title(final_filenames[2], fontsize = 15, weight = 'bold')
                self.completeDF[2].plot(kind = 'barh',
                            legend = False,
                            ax = axes3,
                            x = 'Variables',
                            y = 'Percentage of missing value (%)',
                            xlim = (0,100))
                
                for j in range(self.completeDF[2].shape[0]):
                    if self.completeDF[2].iloc[j,1] == 0:
                        axes3.plot(self.completeDF[2].iloc[j,1], self.completeDF[2].iloc[j,0], 'ok', ms = 10)
                    else:
                        axes3.plot(self.completeDF[2].iloc[j,1], self.completeDF[2].iloc[j,0], 'ok', ms = 0)
                
                axes4 = f.add_subplot(2,3,4)
                axes4.set_title(final_filenames[3], fontsize = 15, weight = 'bold')
                axes4.set_xlabel('Percentage of missing value (%)', fontsize = 15, weight = 'bold')
                axes4.set_ylabel('Variables', fontsize = 15, weight = 'bold')
                self.completeDF[3].plot(kind = 'barh',
                            legend = False,
                            ax = axes4,
                            x = 'Variables',
                            y = 'Percentage of missing value (%)',
                            xlim = (0,100))
                
                for j in range(self.completeDF[3].shape[0]):
                    if self.completeDF[3].iloc[j,1] == 0:
                        axes4.plot(self.completeDF[3].iloc[j,1], self.completeDF[3].iloc[j,0], 'ok', ms = 10)
                    else:
                        axes4.plot(self.completeDF[3].iloc[j,1], self.completeDF[3].iloc[j,0], 'ok', ms = 0)

                axes5 = f.add_subplot(2,3,5, sharey = axes4)
                axes5.set_title(final_filenames[4], fontsize = 15, weight = 'bold')
                axes5.set_xlabel('Percentage of missing value (%)', fontsize = 15, weight = 'bold')
                self.completeDF[4].plot(kind = 'barh',
                            legend = False,
                            ax = axes5,
                            x = 'Variables',
                            y = 'Percentage of missing value (%)',
                            xlim = (0,100))
                
                for j in range(self.completeDF[4].shape[0]):
                    if self.completeDF[4].iloc[j,1] == 0:
                        axes5.plot(self.completeDF[4].iloc[j,1], self.completeDF[4].iloc[j,0], 'ok', ms = 10)
                    else:
                        axes5.plot(self.completeDF[4].iloc[j,1], self.completeDF[4].iloc[j,0], 'ok', ms = 0)
                
                axes6 = f.add_subplot(2,3,6, sharey = axes4)
                axes6.set_title(final_filenames[5], fontsize = 15, weight = 'bold')
                axes6.set_xlabel('Percentage of missing value (%)', fontsize = 15, weight = 'bold')
                self.completeDF[5].plot(kind = 'barh',
                            legend = False,
                            ax = axes6,
                            x = 'Variables',
                            y = 'Percentage of missing value (%)',
                            xlim = (0,100))
                
                for j in range(self.completeDF[5].shape[0]):
                    if self.completeDF[5].iloc[j,1] == 0:
                        axes6.plot(self.completeDF[5].iloc[j,1], self.completeDF[5].iloc[j,0], 'ok', ms = 10)
                    else:
                        axes6.plot(self.completeDF[5].iloc[j,1], self.completeDF[5].iloc[j,0], 'ok', ms = 0)

            elif counter == 7:
                axes1 = f.add_subplot(3,3,1)
                axes1.set_title(final_filenames[0], fontsize = 15, weight = 'bold')
                axes1.set_ylabel('Variables', fontsize = 15, weight = 'bold')
                axes1.set_xlabel('Percentage of missing value (%)', fontsize = 15, weight = 'bold')
                f.subplots_adjust(hspace=0.6)
                self.completeDF[0].plot(kind = 'barh',
                            legend = False,
                            ax = axes1,
                            x = 'Variables',
                            y = 'Percentage of missing value (%)',
                            xlim = (0,100))
                
                for j in range(self.completeDF[0].shape[0]):
                    if self.completeDF[0].iloc[j,1] == 0:
                        axes1.plot(self.completeDF[0].iloc[j,1], self.completeDF[0].iloc[j,0], 'ok', ms = 10)
                    else:
                        axes1.plot(self.completeDF[0].iloc[j,1], self.completeDF[0].iloc[j,0], 'ok', ms = 0)

                axes2 = f.add_subplot(3,3,2, sharey = axes1)
                axes2.set_title(final_filenames[1], fontsize = 15, weight = 'bold')
                axes2.set_xlabel('Percentage of missing value (%)', fontsize = 15, weight = 'bold')
                self.completeDF[1].plot(kind = 'barh',
                            legend = False,
                            ax = axes2,
                            x = 'Variables',
                            y = 'Percentage of missing value (%)',
                            xlim = (0,100))
                
                for j in range(self.completeDF[1].shape[0]):
                    if self.completeDF[1].iloc[j,1] == 0:
                        axes2.plot(self.completeDF[1].iloc[j,1], self.completeDF[1].iloc[j,0], 'ok', ms = 10)
                    else:
                        axes2.plot(self.completeDF[1].iloc[j,1], self.completeDF[1].iloc[j,0], 'ok', ms = 0)

                axes3 = f.add_subplot(3,3,3, sharey = axes1)
                axes3.set_title(final_filenames[2], fontsize = 15, weight = 'bold')
                axes3.set_xlabel('Percentage of missing value (%)', fontsize = 15, weight = 'bold')
                self.completeDF[2].plot(kind = 'barh',
                            legend = False,
                            ax = axes3,
                            x = 'Variables',
                            y = 'Percentage of missing value (%)',
                            xlim = (0,100))

                for j in range(self.completeDF[2].shape[0]):
                    if self.completeDF[2].iloc[j,1] == 0:
                        axes3.plot(self.completeDF[2].iloc[j,1], self.completeDF[2].iloc[j,0], 'ok', ms = 10)
                    else:
                        axes3.plot(self.completeDF[2].iloc[j,1], self.completeDF[2].iloc[j,0], 'ok', ms = 0)
                
                axes4 = f.add_subplot(3,2,3)
                axes4.set_title(final_filenames[3], fontsize = 15, weight = 'bold')
                axes4.set_ylabel('Variables', fontsize = 15, weight = 'bold')
                axes4.set_xlabel('Percentage of missing value (%)', fontsize = 15, weight = 'bold')
                self.completeDF[3].plot(kind = 'barh',
                            legend = False,
                            ax = axes4,
                            x = 'Variables',
                            y = 'Percentage of missing value (%)',
                            xlim = (0,100))
                
                for j in range(self.completeDF[3].shape[0]):
                    if self.completeDF[3].iloc[j,1] == 0:
                        axes4.plot(self.completeDF[3].iloc[j,1], self.completeDF[3].iloc[j,0], 'ok', ms = 10)
                    else:
                        axes4.plot(self.completeDF[3].iloc[j,1], self.completeDF[3].iloc[j,0], 'ok', ms = 0)

                axes5 = f.add_subplot(3,2,4, sharey = axes4)
                axes5.set_title(final_filenames[4], fontsize = 15, weight = 'bold')
                axes5.set_xlabel('Percentage of missing value (%)', fontsize = 15, weight = 'bold')
                self.completeDF[4].plot(kind = 'barh',
                            legend = False,
                            ax = axes5,
                            x = 'Variables',
                            y = 'Percentage of missing value (%)',
                            xlim = (0,100))
                
                for j in range(self.completeDF[4].shape[0]):
                    if self.completeDF[4].iloc[j,1] == 0:
                        axes5.plot(self.completeDF[4].iloc[j,1], self.completeDF[4].iloc[j,0], 'ok', ms = 10)
                    else:
                        axes5.plot(self.completeDF[4].iloc[j,1], self.completeDF[4].iloc[j,0], 'ok', ms = 0)
                
                axes6 = f.add_subplot(3,2,5)
                axes6.set_title(final_filenames[5], fontsize = 15, weight = 'bold')
                axes6.set_ylabel('Variables', fontsize = 15, weight = 'bold')
                axes6.set_xlabel('Percentage of missing value (%)', fontsize = 15, weight = 'bold')
                self.completeDF[5].plot(kind = 'barh',
                            legend = False,
                            ax = axes6,
                            x = 'Variables',
                            y = 'Percentage of missing value (%)',
                            xlim = (0,100))
                
                for j in range(self.completeDF[5].shape[0]):
                    if self.completeDF[5].iloc[j,1] == 0:
                        axes6.plot(self.completeDF[5].iloc[j,1], self.completeDF[5].iloc[j,0], 'ok', ms = 10)
                    else:
                        axes6.plot(self.completeDF[5].iloc[j,1], self.completeDF[5].iloc[j,0], 'ok', ms = 0)
                
                axes7 = f.add_subplot(3,2,6, sharey = axes6)
                axes7.set_title(final_filenames[6], fontsize = 15, weight = 'bold')
                axes7.set_xlabel('Percentage of missing value (%)', fontsize = 15, weight = 'bold')
                self.completeDF[6].plot(kind = 'barh',
                            legend = False,
                            ax = axes7,
                            x = 'Variables',
                            y = 'Percentage of missing value (%)',
                            xlim = (0,100))
                
                for j in range(self.completeDF[6].shape[0]):
                    if self.completeDF[6].iloc[j,1] == 0:
                        axes7.plot(self.completeDF[6].iloc[j,1], self.completeDF[6].iloc[j,0], 'ok', ms = 10)
                    else:
                        axes7.plot(self.completeDF[6].iloc[j,1], self.completeDF[6].iloc[j,0], 'ok', ms = 0)

            elif counter == 8:
                axes1 = f.add_subplot(3,3,1)
                axes1.set_title(final_filenames[0], fontsize = 15, weight = 'bold')
                axes1.set_ylabel('Variables', fontsize = 15, weight = 'bold')
                axes1.set_xlabel('Percentage of missing value (%)', fontsize = 15, weight = 'bold')
                f.subplots_adjust(hspace=0.6)
                self.completeDF[0].plot(kind = 'barh',
                            legend = False,
                            ax = axes1,
                            x = 'Variables',
                            y = 'Percentage of missing value (%)',
                            xlim = (0,100))
                
                for j in range(self.completeDF[0].shape[0]):
                    if self.completeDF[0].iloc[j,1] == 0:
                        axes1.plot(self.completeDF[0].iloc[j,1], self.completeDF[0].iloc[j,0], 'ok', ms = 8)
                    else:
                        axes1.plot(self.completeDF[0].iloc[j,1], self.completeDF[0].iloc[j,0], 'ok', ms = 0)

                axes2 = f.add_subplot(3,3,2, sharey = axes1)
                axes2.set_title(final_filenames[1], fontsize = 15, weight = 'bold')
                axes2.set_xlabel('Percentage of missing value (%)', fontsize = 15, weight = 'bold')
                self.completeDF[1].plot(kind = 'barh',
                            legend = False,
                            ax = axes2,
                            x = 'Variables',
                            y = 'Percentage of missing value (%)',
                            xlim = (0,100))
                
                for j in range(self.completeDF[1].shape[0]):
                    if self.completeDF[1].iloc[j,1] == 0:
                        axes2.plot(self.completeDF[1].iloc[j,1], self.completeDF[1].iloc[j,0], 'ok', ms = 8)
                    else:
                        axes2.plot(self.completeDF[1].iloc[j,1], self.completeDF[1].iloc[j,0], 'ok', ms = 0)

                axes3 = f.add_subplot(3,3,3, sharey = axes1)
                axes3.set_title(final_filenames[2], fontsize = 15, weight = 'bold')
                axes3.set_xlabel('Percentage of missing value (%)', fontsize = 15, weight = 'bold')
                self.completeDF[2].plot(kind = 'barh',
                            legend = False,
                            ax = axes3,
                            x = 'Variables',
                            y = 'Percentage of missing value (%)',
                            xlim = (0,100))
                
                for j in range(self.completeDF[2].shape[0]):
                    if self.completeDF[2].iloc[j,1] == 0:
                        axes3.plot(self.completeDF[2].iloc[j,1], self.completeDF[2].iloc[j,0], 'ok', ms = 8)
                    else:
                        axes3.plot(self.completeDF[2].iloc[j,1], self.completeDF[2].iloc[j,0], 'ok', ms = 0)
                
                axes4 = f.add_subplot(3,3,4)
                axes4.set_title(final_filenames[3], fontsize = 15, weight = 'bold')
                axes4.set_ylabel('Variables', fontsize = 15, weight = 'bold')
                axes4.set_xlabel('Percentage of missing value (%)', fontsize = 15, weight = 'bold')
                self.completeDF[3].plot(kind = 'barh',
                            legend = False,
                            ax = axes4,
                            x = 'Variables',
                            y = 'Percentage of missing value (%)',
                            xlim = (0,100))
                
                for j in range(self.completeDF[3].shape[0]):
                    if self.completeDF[3].iloc[j,1] == 0:
                        axes4.plot(self.completeDF[3].iloc[j,1], self.completeDF[3].iloc[j,0], 'ok', ms = 8)
                    else:
                        axes4.plot(self.completeDF[3].iloc[j,1], self.completeDF[3].iloc[j,0], 'ok', ms = 0)

                axes5 = f.add_subplot(3,3,5, sharey = axes4)
                axes5.set_title(final_filenames[4], fontsize = 15, weight = 'bold')
                axes5.set_xlabel('Percentage of missing value (%)', fontsize = 15, weight = 'bold')
                self.completeDF[4].plot(kind = 'barh',
                            legend = False,
                            ax = axes5,
                            x = 'Variables',
                            y = 'Percentage of missing value (%)',
                            xlim = (0,100))
                
                for j in range(self.completeDF[4].shape[0]):
                    if self.completeDF[4].iloc[j,1] == 0:
                        axes5.plot(self.completeDF[4].iloc[j,1], self.completeDF[4].iloc[j,0], 'ok', ms = 8)
                    else:
                        axes5.plot(self.completeDF[4].iloc[j,1], self.completeDF[4].iloc[j,0], 'ok', ms = 0)

                axes6 = f.add_subplot(3,3,6, sharey = axes4)
                axes6.set_title(final_filenames[5], fontsize = 15, weight = 'bold')
                axes6.set_xlabel('Percentage of missing value (%)', fontsize = 15, weight = 'bold')
                self.completeDF[5].plot(kind = 'barh',
                            legend = False,
                            ax = axes6,
                            x = 'Variables',
                            y = 'Percentage of missing value (%)',
                            xlim = (0,100))
                
                for j in range(self.completeDF[5].shape[0]):
                    if self.completeDF[5].iloc[j,1] == 0:
                        axes6.plot(self.completeDF[5].iloc[j,1], self.completeDF[5].iloc[j,0], 'ok', ms = 8)
                    else:
                        axes6.plot(self.completeDF[5].iloc[j,1], self.completeDF[5].iloc[j,0], 'ok', ms = 0)
            
                axes7 = f.add_subplot(3,2,5)
                axes7.set_title(final_filenames[6], fontsize = 15, weight = 'bold')
                axes7.set_ylabel('Variables', fontsize = 15, weight = 'bold')
                axes7.set_xlabel('Percentage of missing value (%)', fontsize = 15, weight = 'bold')
                self.completeDF[6].plot(kind = 'barh',
                            legend = False,
                            ax = axes7,
                            x = 'Variables',
                            y = 'Percentage of missing value (%)',
                            xlim = (0,100))
                
                for j in range(self.completeDF[6].shape[0]):
                    if self.completeDF[6].iloc[j,1] == 0:
                        axes7.plot(self.completeDF[6].iloc[j,1], self.completeDF[6].iloc[j,0], 'ok', ms = 8)
                    else:
                        axes7.plot(self.completeDF[6].iloc[j,1], self.completeDF[6].iloc[j,0], 'ok', ms = 0)
                
                axes8 = f.add_subplot(3,2,6, sharey = axes7)
                axes8.set_title(final_filenames[7], fontsize = 15, weight = 'bold')
                axes8.set_xlabel('Percentage of missing value (%)', fontsize = 15, weight = 'bold')
                self.completeDF[7].plot(kind = 'barh',
                            legend = False,
                            ax = axes8,
                            x = 'Variables',
                            y = 'Percentage of missing value (%)',
                            xlim = (0,100))

                for j in range(self.completeDF[7].shape[0]):
                    if self.completeDF[7].iloc[j,1] == 0:
                        axes8.plot(self.completeDF[7].iloc[j,1], self.completeDF[7].iloc[j,0], 'ok', ms = 8)
                    else:
                        axes8.plot(self.completeDF[7].iloc[j,1], self.completeDF[7].iloc[j,0], 'ok', ms = 0)
        
            elif counter == 9:
                axes1 = f.add_subplot(3,3,1)
                axes1.set_title(final_filenames[0], fontsize = 15, weight = 'bold')
                axes1.set_ylabel('Variables', fontsize = 15, weight = 'bold')
                f.subplots_adjust(hspace=0.6)
                self.completeDF[0].plot(kind = 'barh',
                            legend = False,
                            ax = axes1,
                            x = 'Variables',
                            y = 'Percentage of missing value (%)',
                            xlim = (0,100))
                
                for j in range(self.completeDF[0].shape[0]):
                    if self.completeDF[0].iloc[j,1] == 0:
                        axes1.plot(self.completeDF[0].iloc[j,1], self.completeDF[0].iloc[j,0], 'ok', ms = 8)
                    else:
                        axes1.plot(self.completeDF[0].iloc[j,1], self.completeDF[0].iloc[j,0], 'ok', ms = 0)

                axes2 = f.add_subplot(3,3,2, sharey = axes1)
                axes2.set_title(final_filenames[1], fontsize = 15, weight = 'bold')
                self.completeDF[1].plot(kind = 'barh',
                            legend = False,
                            ax = axes2,
                            x = 'Variables',
                            y = 'Percentage of missing value (%)',
                            xlim = (0,100))

                for j in range(self.completeDF[1].shape[0]):
                    if self.completeDF[1].iloc[j,1] == 0:
                        axes2.plot(self.completeDF[1].iloc[j,1], self.completeDF[1].iloc[j,0], 'ok', ms = 8)
                    else:
                        axes2.plot(self.completeDF[1].iloc[j,1], self.completeDF[1].iloc[j,0], 'ok', ms = 0)

                axes3 = f.add_subplot(3,3,3, sharey = axes1)
                axes3.set_title(final_filenames[2], fontsize = 15, weight = 'bold')
                self.completeDF[2].plot(kind = 'barh',
                            legend = False,
                            ax = axes3,
                            x = 'Variables',
                            y = 'Percentage of missing value (%)',
                            xlim = (0,100))
                
                for j in range(self.completeDF[2].shape[0]):
                    if self.completeDF[2].iloc[j,1] == 0:
                        axes3.plot(self.completeDF[2].iloc[j,1], self.completeDF[2].iloc[j,0], 'ok', ms = 8)
                    else:
                        axes3.plot(self.completeDF[2].iloc[j,1], self.completeDF[2].iloc[j,0], 'ok', ms = 0)
                
                axes4 = f.add_subplot(3,3,4)
                axes4.set_title(final_filenames[3], fontsize = 15, weight = 'bold')
                axes4.set_ylabel('Variables', fontsize = 15, weight = 'bold')
                self.completeDF[3].plot(kind = 'barh',
                            legend = False,
                            ax = axes4,
                            x = 'Variables',
                            y = 'Percentage of missing value (%)',
                            xlim = (0,100))
                
                for j in range(self.completeDF[3].shape[0]):
                    if self.completeDF[3].iloc[j,1] == 0:
                        axes4.plot(self.completeDF[3].iloc[j,1], self.completeDF[3].iloc[j,0], 'ok', ms = 8)
                    else:
                        axes4.plot(self.completeDF[3].iloc[j,1], self.completeDF[3].iloc[j,0], 'ok', ms = 0)

                axes5 = f.add_subplot(3,3,5, sharey = axes4)
                axes5.set_title(final_filenames[4], fontsize = 15, weight = 'bold')
                self.completeDF[4].plot(kind = 'barh',
                            legend = False,
                            ax = axes5,
                            x = 'Variables',
                            y = 'Percentage of missing value (%)',
                            xlim = (0,100))
                
                for j in range(self.completeDF[4].shape[0]):
                    if self.completeDF[4].iloc[j,1] == 0:
                        axes5.plot(self.completeDF[4].iloc[j,1], self.completeDF[4].iloc[j,0], 'ok', ms = 8)
                    else:
                        axes5.plot(self.completeDF[4].iloc[j,1], self.completeDF[4].iloc[j,0], 'ok', ms = 0)

                axes6 = f.add_subplot(3,3,6, sharey = axes4)
                axes6.set_title(final_filenames[5], fontsize = 15, weight = 'bold')
                self.completeDF[5].plot(kind = 'barh',
                            legend = False,
                            ax = axes6,
                            x = 'Variables',
                            y = 'Percentage of missing value (%)',
                            xlim = (0,100))
                
                for j in range(self.completeDF[5].shape[0]):
                    if self.completeDF[5].iloc[j,1] == 0:
                        axes6.plot(self.completeDF[5].iloc[j,1], self.completeDF[5].iloc[j,0], 'ok', ms = 8)
                    else:
                        axes6.plot(self.completeDF[5].iloc[j,1], self.completeDF[5].iloc[j,0], 'ok', ms = 0)
            
                axes7 = f.add_subplot(3,3,7)
                axes7.set_title(final_filenames[6], fontsize = 15, weight = 'bold')
                axes7.set_ylabel('Variables', fontsize = 15, weight = 'bold')
                axes7.set_xlabel('Percentage of missing value (%)', fontsize = 15, weight = 'bold')
                self.completeDF[6].plot(kind = 'barh',
                            legend = False,
                            ax = axes7,
                            x = 'Variables',
                            y = 'Percentage of missing value (%)',
                            xlim = (0,100))
                
                for j in range(self.completeDF[6].shape[0]):
                    if self.completeDF[6].iloc[j,1] == 0:
                        axes7.plot(self.completeDF[6].iloc[j,1], self.completeDF[6].iloc[j,0], 'ok', ms = 8)
                    else:
                        axes7.plot(self.completeDF[6].iloc[j,1], self.completeDF[6].iloc[j,0], 'ok', ms = 0)
                
                axes8 = f.add_subplot(3,3,8, sharey = axes7)
                axes8.set_title(final_filenames[7], fontsize = 15, weight = 'bold')
                axes8.set_xlabel('Percentage of missing value (%)', fontsize = 15, weight = 'bold')
                self.completeDF[7].plot(kind = 'barh',
                            legend = False,
                            ax = axes8,
                            x = 'Variables',
                            y = 'Percentage of missing value (%)',
                            xlim = (0,100))
                
                for j in range(self.completeDF[7].shape[0]):
                    if self.completeDF[7].iloc[j,1] == 0:
                        axes8.plot(self.completeDF[7].iloc[j,1], self.completeDF[7].iloc[j,0], 'ok', ms = 8)
                    else:
                        axes8.plot(self.completeDF[7].iloc[j,1], self.completeDF[7].iloc[j,0], 'ok', ms = 0)

                axes9 = f.add_subplot(3,3,9, sharey = axes7)
                axes9.set_title(final_filenames[8], fontsize = 15, weight = 'bold')
                axes9.set_xlabel('Percentage of missing value (%)', fontsize = 15, weight = 'bold')
                self.completeDF[8].plot(kind = 'barh',
                            legend = False,
                            ax = axes9,
                            x = 'Variables',
                            y = 'Percentage of missing value (%)',
                            xlim = (0,100))

                for j in range(self.completeDF[8].shape[0]):
                    if self.completeDF[8].iloc[j,1] == 0:
                        axes9.plot(self.completeDF[8].iloc[j,1], self.completeDF[8].iloc[j,0], 'ok', ms = 8)
                    else:
                        axes9.plot(self.completeDF[8].iloc[j,1], self.completeDF[8].iloc[j,0], 'ok', ms = 0)          

def main():
    root = tk.Tk()
    run = App(root)
    root.mainloop()

if __name__ == '__main__':
    main()